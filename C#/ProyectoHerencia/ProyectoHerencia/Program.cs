﻿using System;

namespace ProyectoHerencia
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Caballo c1 = new Caballo("Manin");
            Humano h1 = new Humano("Daniel");
            Gorila g1 = new Gorila("Copito");

            Mamiferos animal = new Caballo("Pegasso");
            Mamiferos persona = new Mamiferos("Daniel");

            Mamiferos animal2 = new Mamiferos("Bucefalo");
            Caballo Bucefalo = new Caballo("Bucefalo");

            animal2 = Bucefalo;
            Object miAnimal = new Caballo("Bucefalo");
            Object miPersona = new Humano("Daniel");
            Object miMamifero = new Mamiferos("Wally");

            Mamiferos[] almacenAnimales = new Mamiferos[3];
            almacenAnimales[0] = c1;
            almacenAnimales[1] = h1;
            almacenAnimales[2] = g1;

            for (int i=0; i<3; i++)
            {
                almacenAnimales[i].pensar();
            }
            
            /*c1.getNombre();
            h1.getNombre();
            g1.getNombre();*/
        }
    }

    class Mamiferos
    {
        private String nombreSerVivo;


        public Mamiferos(String nombreSerVivo)
        {
            this.nombreSerVivo = nombreSerVivo;
        }

        public void respirar() => Console.WriteLine("Soy capaz de respirar");

        public virtual void pensar() => Console.WriteLine("Pensamiento basico instintivo");

        public void cuidarCrias() => Console.WriteLine("Cuido de mis crias hasta que se valgan por si solas");

        public void getNombre() => Console.WriteLine($"El nombre  es {nombreSerVivo}");

    }

    class Caballo : Mamiferos
    {
        public Caballo(String nombreCaballo) : base(nombreCaballo) { }

        public void galopar() => Console.WriteLine("Soy capaz de galopar");
    }

    class Humano : Mamiferos
    {
        public Humano(String nombreHumano) : base(nombreHumano) { }

        public override void pensar() => Console.WriteLine("Soy capaz de pensar ¿?");
    }

    class Gorila : Mamiferos
    {
        public Gorila(String nombreGorila) : base(nombreGorila) { }

        public void Trepar() => Console.WriteLine("Soy capaz de trepar");

        public override void pensar() => Console.WriteLine("Pensamientgo instintivo avanzado");
    }
}