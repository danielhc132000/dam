﻿using System;

namespace UsoArray
{
    internal class Program
    {
        static void Main(string[] args)
        {
            //  Array implicito

            var valores = new[] { 15, 28, 35, 75.5, 30.30 };

            //  Array de objetos

            Empleados[] arrayEmpleados=new Empleados[3];
            arrayEmpleados[0] = new Empleados("Daniel", 22);
            
            Empleados saraEmpleado = new Empleados("Sara", 27);
            arrayEmpleados[1] = saraEmpleado;

            arrayEmpleados[2] = new Empleados("Manuel", 51);

            //  Array de clases anonimas

            var personas = new[]
            {
                new { Nombre = "Alba", Edad = 23 },
                new { Nombre = "Diana", Edad = 29 },
                new { Nombre = "Juan", Edad = 19 }
            };

            Console.WriteLine(personas[1].Nombre);

            //for(int i=0; i<arrayEmpleados.Length; i++)
            //    Console.WriteLine(arrayEmpleados[i].getInfo());

            foreach (var valor in personas)
            {
                Console.WriteLine(valor);
            }

        }
    }

    class Empleados
    {
        private String nombre;
        private int edad;

        public Empleados(String nombre, int edad)
        {
            this.nombre = nombre;
            this.edad = edad;
        }

        public String getInfo()
        {
            return $"Nombre: {this.nombre}\nEdad: {this.edad}\n";
        }
    }
}