﻿using System;


namespace ConceptosPOO
{
    internal class Program
    {
        static void Main(string[] args)
        {
            realizarTarea();

            var MiVariable = new { Nombre = "Daniel", Edad = 22 };

            Console.WriteLine(MiVariable.Nombre + " " + MiVariable.Edad);

            var miOtraVariable = new { Nombre = "Ana", Edad = 25 };

            MiVariable = miOtraVariable;
        }

        static void realizarTarea()
        {
            Punto origen = new Punto();
            Punto destino = new Punto(150, 90);
            Punto otroPunto = new Punto();

            double distanciaFinal = origen.DistanciaHasta(destino);

            Console.WriteLine($"La distancia entre los dos puntos es de: {distanciaFinal}");

            Console.WriteLine($"Numero de objetos creados: {Punto.getContadorDeObjetos()}");
        }
    }
}
