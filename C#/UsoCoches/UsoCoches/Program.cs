﻿using System;

namespace UsoCoches
{
    class Program
    {
        static void Main(string[] args)
        {
            Coche coche = new Coche(4, 8, 4);

            Console.WriteLine(coche.getInfo());
            coche.setExtras(true, "Tela gris Deportiva");
            Console.WriteLine(coche.getExtras());

            Coche coche2 = new Coche(2, 3, 2);

            Console.WriteLine(coche2.getInfo());
            coche2.setExtras(true);
            Console.WriteLine(coche2.getExtras());
        }
    }

    partial class Coche     //  Atributos y constructor de la clase Coche
    {
        private int ruedas;
        private double largo;
        private double ancho;
        private bool climatizador;
        private String tapiceria;

        public Coche(int ruedas, double largo, double ancho)
        {
            this.ruedas = ruedas;
            this.largo = largo;
            this.ancho = ancho;
        }

    }

    partial class Coche {       //  Metodos pertenecientes a la clase Coche

        public string getInfo()
        {
            return "Informacion del coche:\n" + "Ruedas: " + ruedas + "\nLargo: " + largo + "\nAncho: " + ancho;
        }

        public string getExtras()
        {
            return "\nExtras: " + "\nClimatizador: " + climatizador + "\nTapiceria: " + tapiceria;
        }


        public void setExtras(bool climatizador, String tapiceria = "Tela")
        {
            this.climatizador = climatizador;
            this.tapiceria = tapiceria;
        }

    }

}
