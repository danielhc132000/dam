####################
#   CLASE PADRE    #
####################


class Vehiculo():
    
    def __init__(self, color, ruedas):
        self.color = color
        self.ruedas = ruedas
        
    def __str__(self):
        return "color {}, {} ruedas".format( self.color, self.ruedas )
        
        
##########################
#   CLASES RAMA COCHE    #
##########################        


class Coche(Vehiculo):
    
    def __init__(self, color, ruedas, velocidad, cilindrada):
        super().__init__(color, ruedas)  # utilizamos super() sin self en lugar de Vehiculo
        self.velocidad = velocidad
        self.cilindrada = cilindrada
        
    def __str__(self):
        return super().__str__() + ", {} km/h, {} cc".format(self.velocidad, self.cilindrada)


class Camioneta(Coche):
    def __init__(self, color, ruedas, velocidad, cilindrada, carga):
        super().__init__(color, ruedas, velocidad, cilindrada)
        self.carga = carga
        
    def __str__(self):
        return super().__str__() + f", {self.carga} kg"


##############################
#   CLASES RAMA BICICLETA    #
##############################


class Bicicleta(Vehiculo):
    def __init__(self, color, ruedas, tipo):
        super().__init__(color, ruedas)
        self.tipo = tipo

    def __str__(self):
        return super().__str__() + f", tipo {self.tipo}"

    
class Motocicleta(Bicicleta):
    def __init__(self, color, ruedas, tipo, velocidad, cilindrada):
        super().__init__(color, ruedas, tipo)
        self.velocidad = velocidad
        self.cilindrada = cilindrada
        
    def __str__(self):
        return super().__str__() + f", {self.velocidad} km/h, {self.cilindrada} cc"

    
###########################
#   FUNCIONES GLOBALES    #
###########################

def catalogo(vehiculos):
    for element in vehiculos:
        print(type(element).__name__ + " : " + str(element))

    
def catalogar(vehiculos, ruedas=None):
    print()
    if ruedas is not None:
        num = 0
        for element in vehiculos:
            if element.ruedas == ruedas:
                num+=1
                print(type(element).__name__ + " : " + str(element))
        print(f"Se han encontrado {num} vehiculos con {ruedas} ruedas")


##################
#    PRUEBAS     #
##################

camioneta = Camioneta("Blanca", 4, 120, 650, 1200)
bici = Bicicleta("Negra", 2, "Deportiva")
moto = Motocicleta("Verde", 2, "Urbana", 260, 1600)

vehiculos = [camioneta, bici, moto]

catalogo(vehiculos)
catalogar(vehiculos, 2)
