tareas = [ 
    [6, 'Distribución'],
    [2, 'Diseño'],
    [1, 'Concepción'],
    [7, 'Mantenimiento'],
    [4, 'Producción'],
    [3, 'Planificación'],
    [5, 'Pruebas']
]

print("==Tareas desordenadas==")
for tarea in tareas:
    print(tarea[0], tarea[1])


from collections import deque

print("\n==Tareas ordenadas==")
tareas.sort() # Ordena los elementos de la lista

cola = deque()
for tarea in tareas:
    cola.append(tarea[1])

for i, tarea in enumerate(cola):
    print(i+1, tarea)
