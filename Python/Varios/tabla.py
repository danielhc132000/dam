import sys

def imprimir():
    for fil in range(0,filas+1):
        for col in range(0,columnas+1):
            if fil == 0 or col == 0  or fil == filas or col == columnas:
                print("* ", end="")
                continue
            print("  ", end="")
        print("")


if len(sys.argv) == 3:
    filas = int(sys.argv[1])
    columnas = int(sys.argv[2])
    imprimir()
else:
    print("Guia de uso del script")
    print(" tabla.py \"numero_filas\" \"numero_columnas\" ")

