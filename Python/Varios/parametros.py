def double_value(number):  # Plain data has pass by value
    number *= 2


n = 10
double_value(n)
print(n)


def double_values(numbers):  # List has pass by reference
    for i, n in enumerate(numbers):
        numbers[i] *= 2


ns = [10, 50, 100]
double_values(ns)
print(ns)
