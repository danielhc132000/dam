tareas = [ 
    [6, 'Distribución'],
    [2, 'Diseño'],
    [1, 'Concepción'],
    [7, 'Mantenimiento'],
    [4, 'Producción'],
    [3, 'Planificación'],
    [5, 'Pruebas']
]

print("==Tareas desordenadas==")
for tarea in tareas:
    print(tarea[0], tarea[1])

# Completa el ejercicio aquí

print("\n==Tareas ordenadas==")

cola_tareas = []
prioridad = 1

for i in range(len(tareas)):
    for tarea_pos in tareas:   
        if prioridad == tarea_pos[0]:
            cola_tareas.append(tarea_pos[1])
            prioridad+=1

for i, tarea in enumerate(cola_tareas):
    print(i+1,tarea)
