usuarios = {"Marta", "David", "Elvira", "Juan", "Marcos"}
administradores = {"Juan", "Marta"}

administradores.remove("Juan")
administradores.add("Marcos")

print("USUARIOS: ",usuarios)
print("ADMINISTRADORES: ", administradores, "\n")

for usuario in usuarios:
    if usuario in administradores:
        print(usuario+" es un administrador")
    else:
        print(usuario+" es un usuario normal")