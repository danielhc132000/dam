import math


#################
#   CLASE PUNTO #
#################


class Punto:
   
    def __init__(self, x=0, y=0):
        self.x = x
        self.y = y
        
    def __str__(self):
        return(f'({self.x},{self.y})')
        
    def cuadrante(self):
        if self.x > 0 and self.y > 0:
            print(f"{self} se encuentra en el primer cuadrante")
        elif self.x < 0 and self.y < 0:
            print(f"{self} se encuentra en el segundo cuadrante")
        elif self.x < 0 and self.y < 0:
            print(f"{self} se encuentra en el tercer cuadrante")
        elif self.x > 0 and self.y < 0:
            print(f"{self} se encuentra en el cuarto cuadrante")
        else:
            print(f"{self} se encuentra en el punto de origen")

    def vector(self, punto):
        print(f"El vector entre {self} y {punto} es ({punto.x - self.x},{punto.y - self.y})")
    
    def distancia(self, punto):
        d = math.sqrt( (self.x-punto.x)**2 + (self.y - punto.y)**2 )
        print(f"La distancia entre los puntos {self} y {punto} es {d:.4}")


#########################
#   CLASE RECTANGULO    #
#########################


class Rectangulo:

    def __init__(self, pInicial = Punto(), pFinal = Punto() ):
        self.pInicial = pInicial
        self.pFinal = pFinal
        
    def base(self):
        self.base = abs(self.pFinal.x - self.pInicial.x)
        print(f"La base del rectangulo es {self.base}")
        
    def altura(self):
        self.altura = abs(self.pFinal.y - self.pInicial.y)
        print(f"La altura del rectangulo es {self.altura}")
        
    def area(self):
        self.area = self.base * self.altura
        print("El area del rectangulo es {}".format(self.area))



#####################
#   PRUEBAS PUNTO   #
#####################


A = Punto(2,3)
B = Punto(5,5)
C = Punto(-3,-1)
D = Punto(0,0)

A.cuadrante()
C.cuadrante()
D.cuadrante()

A.vector(B)
B.vector(A)

A.distancia(D)
B.distancia(D)
C.distancia(D)



###########################
#   PRUEBAS RECTANGULO    #
###########################


R = Rectangulo(A,B)
R.base()
R.altura()
R.area()    