#include <iostream>
#include <stdlib.h>


struct Nodo {
    int dato;
    Nodo *siguiente;
};


bool voidQueue (Nodo *frente) { return frente==NULL ? true : false; }


void insertQueue (Nodo *&frente, Nodo *&fin, int _dato){

    Nodo *nuevo_nodo = new Nodo();
    nuevo_nodo->dato = _dato;
    nuevo_nodo->siguiente = NULL;

    if(voidQueue(frente)){
        frente = nuevo_nodo;
    }else{
        fin->siguiente = nuevo_nodo;
    }

    fin = nuevo_nodo;

    std::cout<<"Numero "<<_dato<<" agregado a la Cola.\n\n";

}


void removeQueue(Nodo *&frente, Nodo *&fin, int &_dato){
    _dato = frente->dato;
    Nodo *aux = frente;

    if(frente == fin){  //  Solo hay un nodo
        frente = NULL;
        fin = NULL;
    }else{  //  Hay mas de un elemento
        frente = frente->siguiente;
    }

    delete aux;
}

int main (int argc, char **argv){

    Nodo *frente = NULL;
    Nodo *fin = NULL;
    int dato;

    std::cout<<"Numero a agregar a la Cola: ";
    std::cin>>dato;
    insertQueue(frente, fin, dato);

    std::cout<<"Numero a agregar a la Cola: ";
    std::cin>>dato;
    insertQueue(frente, fin, dato);

    std::cout<<"Numero a agregar a la Cola: ";
    std::cin>>dato;
    insertQueue(frente, fin, dato);


    while(frente != NULL){
        removeQueue(frente, fin, dato);
        if(frente!=NULL)
            std::cout<<dato<<", ";
        else
            std::cout<<dato<<".";
    }



    return EXIT_SUCCESS;
}
