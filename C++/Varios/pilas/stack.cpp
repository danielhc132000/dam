#include <iostream>
#include <stack>

using namespace std;


int main (){

  stack<int> stack;

  cout<<"Estado de la pila: "<<stack.empty()<<"\n";
  stack.push(12);
  stack.push(34);

  cout<<"Estado de la pila: "<<stack.empty()<<"\n";

  cout<<"El tamaño de la pila es: "<<stack.size();

  cout<<"\nElemento agragado: "<<stack.top();
  stack.pop();

  cout<<"\nEstado de la pila: "<<stack.empty()<<"\n";

  cout<<"\nElemento agragado: "<<stack.top();
  stack.pop();

  cout<<"\nEstado de la pila: "<<stack.empty()<<"\n";

  return EXIT_SUCCESS;
}

/*
 stack.empty()-->Indica con un 1 si la pila esta vacia y con un 0 si tiene elementos
 stack.size()--->Indica la cantidad de elementos que hay en la pila
 stack.push()--->Sirve para introducir valores dentro de la pila
 stack.pop()---->Sirve para eliminar el ultimo elemento agregado a la pila
 stack.top()---->Retorna el ultimo elemento agregado a la pila
*/
