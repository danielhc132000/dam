#include <iostream>
#include <stdlib.h>


struct Nodo {
    int dato;
    Nodo *siguiente;
};


void
agregarPila(Nodo *&pila, int n){
    Nodo *nuevo_nodo = new Nodo();
    nuevo_nodo->dato = n;
    nuevo_nodo->siguiente = pila;
    pila = nuevo_nodo;

    std::cout<<"Elemento "<<n<<" agregado a la PILA correctamente.\n\n";
}


void
sacarPila(Nodo *&pila, int &n){ //  Usamos puntero por referencia porque queremos cambiar el valor del puntero y no a las variables a las que apunta
    Nodo *aux = pila;
    n = aux->dato;
    pila = aux->siguiente;
    delete aux;
}

int
main(int argc, char **argv){

    Nodo *pila = NULL;
    int dato;

    std::cout<<"Numero: ";
    std::cin>>dato;
    agregarPila(pila, dato);
    
    std::cout<<"Otro Numero: ";
    std::cin>>dato;
    agregarPila(pila, dato);

    std::cout<<"Sacando los elementos de la pila: ";
    
    while(pila != NULL){    //  Mientras no sea el final de la pila
        sacarPila(pila, dato);
        if(pila!=NULL){
            std::cout<<dato<<", ";
        }else{
            std::cout<<dato<<".";
        }
    }


    free(pila);

    return EXIT_SUCCESS;
}
