#include <iostream>
#include <stdlib.h>
#include <unistd.h>

const char *menu[]{
    "Insertar un caracter a la pila.",
    "Mostrar todos los elementos de la pila.",
    "Salir.",
    NULL
};


struct Nodo {
    
    char caracter;
    Nodo *next;
};


void
print_menu(){

    for(int i=0; menu[i]!=NULL; i++)
        std::cout<<i+1<<". "<<menu[i]<<"\n";
}


void
opciones(int &opcion){
    print_menu();
    std::cout<<"\nSeleccione la opcion: ";
    std::cin>>opcion;
}


void
selectChar(char &_letra){

    std::cout<<"Letra a agregar: ";
    std::cin>>_letra;
}


void
addStack(Nodo *&cima, char _letra){

    Nodo *nuevo_nodo = new Nodo();
    nuevo_nodo->caracter = _letra;
    nuevo_nodo->next = cima;
    cima = nuevo_nodo;
}


void
showStack(Nodo *&cima, char &letra){
    Nodo *aux = cima;
    letra = cima->caracter;
    cima = aux->next;
    delete aux;
}



int
main(int argc, char **argv){

    Nodo *cima = NULL;
    int opcion;
    char letra;

    do{
        opciones(opcion);
        
        switch(opcion){
            case 1:
                selectChar(letra);
                addStack(cima, letra);
                std::cout<<"\n\n";
                break;
            case 2:
                for(int i=0; cima!=NULL; i++){
                    showStack(cima, letra);
                    std::cout<<letra;
                }
        }
    }while(opcion<2);


    return EXIT_SUCCESS;
}
