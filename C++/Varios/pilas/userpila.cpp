#include <iostream>
#include <stdlib.h>


struct Nodo{
    int dato;
    Nodo *next;
};


void
agregarPila(Nodo *&pila, int n){

    Nodo *nuevo_nodo = new Nodo();
    nuevo_nodo->dato = n;
    nuevo_nodo->next = pila;
    pila = nuevo_nodo;
}

void
sacarPila(Nodo *&pila, int &n){
    Nodo *aux = pila;
    n = aux->dato;
    pila = aux->next;
    delete aux;
}

int
main(int argc, char **argv){

    Nodo *pila = NULL;
    int numero;
    char respuesta;

    do{

        std::cout<<"Numero: ";
        std::cin>>numero;
        agregarPila(pila, numero);

        std::cout<<"Agregar nuevo dato a la PILA?(s/N): ";
        std::cin>>respuesta;
    }while((respuesta =='S') || (respuesta =='s'));


    std::cout<<"Sacando elementos de la PILA: ";
    while(pila != NULL){
        sacarPila(pila, numero);
        if(pila!=NULL){
            std::cout<<numero<<", ";
        }else{
            std::cout<<numero<<".";
        }
    }


    return EXIT_SUCCESS;
}
