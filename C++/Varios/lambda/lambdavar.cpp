#include <iostream>
#include <array>
#include <algorithm>
#include <string_view>


int main(){

    constexpr std::array<std::string_view, 4> arr{"amarillo", "naranja", "verde", "rojo"};

    std::string cadena{};

    std::cout<<"Buscar: ";
    std::cin>>cadena;

    auto find_ver{
        [cadena](std::string_view str)
        {
            return (str.find(cadena) != std::string_view::npos);
        }
    };

    const auto encontrado {std::find_if(arr.begin(), arr.end(), find_ver)};

    if(encontrado == arr.end())
        std::cout << "No se encontro: "<<cadena<<"\n";
    else
        std::cout << "Encontrado es: " << *encontrado << '\n';


    return 0;
}
