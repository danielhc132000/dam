#include <iostream>
#include <array>
#include <algorithm>
#include <string_view>


int main(){

    constexpr std::array<std::string_view, 4> arr{"amarillo", "naranja", "verde", "rojo"};

    const auto encontrado {std::find_if(arr.begin(), arr.end(),
            [](std::string_view str){
                return (str.find("ver") != std::string_view::npos);
            })};

    if(encontrado == arr.end())
        std::cout << "No se encontro ver\n";
    else
        std::cout << "Encontrado es: " << *encontrado << '\n';


    return 0;
}
