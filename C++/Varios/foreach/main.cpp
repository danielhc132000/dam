#include <iostream>
#include <string>
#include <vector>

int main(){

    //std::string array[]{"Dani", "tuvo", "la mejor", "nota"};
    std::vector<std::string> array{"Dani", "saco", "la mejor", "nota"};

    for(const auto& valores : array){
        std::cout << valores << ' ';
    }

    return 0;
}
