#ifndef LANDFILL_HPP
#define LANDFILL_HPP

#include "car.hpp"
#include "vehi.hpp"
#include "moto.hpp"

class LandFill {
	public:
	void dumpCar(Vehicle &v){
		v.setWheels(0);
		v.setDoors(0);
	}
	void dumpMoto(Vehicle *v){
		v->setWheels(0);
		v->setDoors(0);
	}

};

#endif
