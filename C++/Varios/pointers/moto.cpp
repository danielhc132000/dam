#include"moto.hpp"
#include<iostream>

Moto::Moto(bool automatic, int wheels, int doors, bool radio, bool helmetCase) : Vehicle(automatic, wheels, doors, radio) 
{
	this->helmetCase = helmetCase;
}

void Moto::acel() {
	std::cout<<"The moto is accelerating. \n";
}


