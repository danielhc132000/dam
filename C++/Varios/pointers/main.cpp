#include"car.hpp"
#include"moto.hpp"
#include"landfill.hpp"
#include<iostream>

using namespace std;

int
main ()
{

  Vehicle *p[2];
  LandFill f;

  int **pf = (int **) malloc (2 * sizeof (int *));
  *pf = (int *) malloc (1 * sizeof (int));
  *(pf + 1) = (int *) malloc (1 * sizeof (int));

  int w[2]
  {
  4, 3};

  p[0] = new Car (true, 4, 4, false);
  p[1] = new Moto (true, 2, 1, true, true);

  for (int i = 0; i < 2; i++)
    p[i]->acel ();

  cout << "The Car has: " << p[0]->getWheels () << " wheels and " << p[0]->
    getDoors () << " doors. \n";
  cout << "The Moto has: " << p[1]->getWheels () << " wheels and " << p[1]->
    getDoors () << " doors. \n";

  f.dumpCar (**p);		    // **p = *p[1] !=  *p
  f.dumpMoto (*(*&p + 1));	// *(*&p+1) = p[1] != &(p+1) y *(&p+1)

  cout << "The Car has: " << p[0]->getWheels () << " wheels and " << p[0]->
    getDoors () << " doors. \n";
  cout << "The Moto has: " << p[1]->getWheels () << " wheels and " << p[1]->
    getDoors () << " doors. \n\n";

  cout << w[1];
  cout << *(w + 1) << "\n";

  **pf = 4;
  **(pf + 1) = 6;

  cout << **pf;
  cout << **(pf + 1);


  return 0;
}
