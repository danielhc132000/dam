#ifndef VEHI_HPP
#define VEHI_HPP

class Vehicle {
	protected:
	
	bool automatic;
	int wheels;
	int doors;
	bool radio;

	public:

	Vehicle(bool, int, int, bool);
	
	bool getAtomatic(){return this->automatic;};
	int getWheels(){return this->wheels;};
	int getDoors(){return this->doors;};
	bool getRadio(){return this->radio;};

	void setWheels(int x) {this->wheels = x;};
	void setDoors(int x) {this->doors = x;};

	virtual void acel() = 0;
	int decel(){return 1;};

};

#endif
