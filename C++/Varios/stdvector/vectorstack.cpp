#include <iostream>
#include <vector>


void
imprimirStack(std::vector<int> &stack){
    for(int i : std::vector<int>(stack)){
        std::cout<<i<<" ";
    }
    std::cout<<"(size: "<<stack.size()<<" capacity: "<<
        stack.capacity()<<" )\n";
}



int main(){

    std::vector<int> stack;

    //stack.reserve(5);     Reserva una capacidad inicial

    stack.push_back(5); //  Agrega un elemento a la cima de la pila
    imprimirStack(stack);

    stack.push_back(3);
    imprimirStack(stack);

    stack.push_back(2);
    imprimirStack(stack);

    std::cout<<"Elemento en la parte superior de la pila: "<<stack.back() << '\n';

    imprimirStack(stack);
   
    stack.pop_back();
    imprimirStack(stack);

    stack.pop_back();
    imprimirStack(stack);

    
    return 0;
}
