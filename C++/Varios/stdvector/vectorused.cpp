#include <iostream>
#include <vector>


void
imprimirTam(const std::vector<int> &array){
    setlocale(LC_ALL, "es_ES.UTF-8");
    std::cout << "El tamaño es: " << array.size() << '\n';
}


int main(){
    
    std::vector array{9, 7, 5, 3, 1};
    std::cout<<"Vector Array:\n";
    
    array.resize(10);
    imprimirTam(array);
    std::cout<<"La capacidad es: " << array.capacity() << '\n'; 

    std::cout<<"\nArray: ";
    for(auto elementos : array){
        std::cout << elementos << ' ';
    }


    return 0;
}
