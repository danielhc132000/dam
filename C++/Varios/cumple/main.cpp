#include <iostream>
#include <stdlib.h>

#include "Dianio.h"


int
main(int argc, char **argv){

    Dianio *hoy;
    Dianio *cumple;
    int dia, mes;

    std::cout<<"Que dia es hoy?: ";
    std::cin>>dia;

    std::cout<<"En que mes estamos?: ";
    std::cin>>mes;

    hoy = new Dianio(dia, mes);

    std::cout<<"Que dia es tu cumple?: ";
    std::cin>>dia;

    std::cout<<"En que mes es tu cumple?: ";
    std::cin>>mes;

    cumple = new Dianio(dia, mes);

    if(hoy->igual(*cumple))
        std::cout<<"Felicidades!!";
    else
        std::cout<<"Que tenga un buen dia.";



    return EXIT_SUCCESS;
}
