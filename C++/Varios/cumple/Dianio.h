#ifndef DIANIO_H
#define DIANIO_H

class Dianio {

    private:
        int dia;
        int mes;

    public:
        Dianio (int, int);
        bool igual (Dianio& d);
        void ver ();
};


#endif
