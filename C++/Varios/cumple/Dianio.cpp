#include <iostream>
#include "Dianio.h"


Dianio::Dianio (int _dia, int _mes) : dia(_dia), mes(_mes) {}

bool Dianio::igual (Dianio& d){
    if(dia == d.dia && mes==d.mes)
        return true;
    else
        return false;
}


void Dianio::ver (){
    std::cout<<"Mes: "<<mes;
    std::cout<<"Dia: "<<dia;
}
