#include <iostream>
#include "class.hpp"

using namespace std;

int main(int argc, char const *argv[]){
    
    EjemploPlantillas <int, float> obj1(5, 9.87);   //  Indicas antes de instanciar el objeto, los tipos de datos a recibir

    obj1.mostrarDatos();

    cout<<endl;

    obj1.setDato1(1756);
    obj1.setDato2(4.35);
    obj1.mostrarDatos();


    return 0;
}
