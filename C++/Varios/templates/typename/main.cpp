#include <iostream>

using namespace std;
template <typename Tipo>    //  typename se recomienda usar para templates de funciones aunque tambien funciona class(recomendado para clases)


Tipo mayor(Tipo dato1, Tipo dato2){  //  El template solo tiene un class por lo que solo puede acceptar un tipo de dato
    if (dato1>dato2)
        return dato1;
    return dato2;
}


int main(int argc, char const *argv[]){
    
    cout<<"El mayor entero es: "<<mayor(5,9)<<endl;
    cout<<"El mayor double es: "<<mayor(5.46,9.27)<<endl;
    cout<<"El mayor caracter es: "<<mayor('r','z')<<endl;


    return 0;
}
