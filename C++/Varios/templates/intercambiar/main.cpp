#include <iostream>
#include "trade.hpp"

using namespace std;


int main(){
    string dato1, dato2;

    cout<<"Escriba dato1: ";
    cin>>dato1;

    cout<<"Escriba dato2: ";
    cin>>dato2;

    trade(dato1, dato2);

    cout<<"El valor de n1 es: "<<dato1<<endl;
    cout<<"El valor de n2 es: "<<dato2;

    return 0;
}