#include <iostream>
#include "trade.hpp"

using namespace std;

template <typename T>
void ordenarAscendente(T *array, int n){
    for(int i=0; i<n; i++)
        for(int j=0; j<n; j++)
            if(array[j] > array[j+1])
                trade(array[j], array[j+1]);
}


template <typename T>
void ordenarDescendente(T *array, int n){
    for(int i=0; i<n; i++)
        for(int j=0; j<n; j++)
            if(array[j] < array[j+1])
                trade(array[j], array[j+1]);
}