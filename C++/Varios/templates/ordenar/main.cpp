#include <iostream>
#include "ordenar.hpp"

using namespace std;


template <typename T>
void pedirDatos(T *array, int n){
    for(int i=0; i<n; i++){
        cout<<"Numero: ";
        cin>>array[i];
    }
}

template <typename T>
void verDatos(T *elementos, int n){
    for(int i=0; i<n; i++)
        cout<<elementos[i]<<" ";
}


int main(int argc, char const *argv[]){
    
    int nElementos;

    cout<<"Numero de elementos para la lista: ";
    cin>>nElementos;

    int *elementos = new int[nElementos];

    cout<<"Pidiendo datos para la lista: \n\n";
    pedirDatos(elementos, nElementos);

    ordenarAscendente(elementos, nElementos);
    cout<<"Ordenados Ascendentemente: ";
    verDatos(elementos, nElementos);
    

    ordenarDescendente(elementos, nElementos);
    cout<<"\nOrdenados Descendentemente: ";
    verDatos(elementos, nElementos);
 

    return 0;
}
