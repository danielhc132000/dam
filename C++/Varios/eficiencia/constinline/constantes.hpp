#pragma once
#ifndef CONSTANTS_HPP
#define CONSTANTS_HPP

namespace constantes
{
    inline constexpr double pi{ 3.14159 };
    inline constexpr double avogrado{ 6.0221413e23 };
    inline constexpr double myGravity{ 9.2 };

    inline int ver(){ return 10; }
}

#endif