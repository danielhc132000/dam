#include "constantes.hpp"
#include <iostream>


int main(int argc, char const *argv[])
{
    int radio{};

    std::cout<<"Escribe un radio: ";
    std::cin>>radio;

    std::cout<<"La circunferencia es: "<<2.0 * radio * constantes::pi<<"\n";

    std::cout<<constantes::ver();

    return 0;
}
