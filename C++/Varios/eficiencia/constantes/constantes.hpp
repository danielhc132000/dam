#pragma once
#ifndef CONSTANTS_HPP
#define CONSTANTS_HPP

namespace constantes
{
    extern const double pi;
    extern const double avogrado;
    extern const double miGravedad;
}

#endif