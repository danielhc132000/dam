#include "constantes.hpp"

namespace constantes 
{
    extern const double pi{ 3.14159 };
    extern const double avogrado{ 6.0221413e23 };
    extern const double miGravedad{ 9.3 };
}