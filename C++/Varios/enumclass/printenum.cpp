#include <iostream>


enum class Colores{
    rojo,
    verde,
    azul
};


int main(){

    std::cout<<static_cast<int>(Colores::azul);
    //  Importante indicar el ambito y castear el valor ya que el compilador no sabe imprimir valores enum.


    return 0;
}
