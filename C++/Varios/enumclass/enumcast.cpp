#include <iostream>


enum class Tipo{
    tInt,
    tFloat,
    tCString
};

//  No recomendable usarlos a no ser que sea obligatorio, recomendable usar mejor templates.
void
imprimirValor(void *ptr, Tipo tipo){    //  Punteros void pueden apuntar a cualquier tipo de dato.
    switch(tipo) {
        case Tipo::tInt:
            std::cout<<*static_cast<int*>(ptr) << '\n'; //  Indireccionamos a la vez que casteamos
            break;
        case Tipo::tFloat:
            std::cout<<*static_cast<float*>(ptr) << '\n';
            break;
        case Tipo::tCString:
            std::cout<<static_cast<char*>(ptr) << '\n';
            break;
        default:
            std::cout << "Tipo no encontrado\n";
    }
}



int main(){

    int nValor{3};
    float fValor{1.5f};
    char szValor[]{"Daniel"};

    
    imprimirValor(&nValor, Tipo::tInt);
    imprimirValor(&fValor, Tipo::tFloat);
    imprimirValor(&szValor, Tipo::tCString);


    return 0;
}
