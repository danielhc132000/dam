#include <iostream>
#include <array>


struct Casa{
    int numero{};
    int pisos{};
    int vivPorPiso{};
};


int main(){

    std::array<Casa, 4> casas{};

    casas[0] = {13, 4, 30};
    casas[1] = {14, 3, 10};
    casas[2] = {15, 3, 40};
    casas[3] = {16, 2, 12};

    for(const auto &viviendas : casas)
    {
        std::cout<<"Casa num. " << viviendas.numero
            << "tiene " << (viviendas.pisos * viviendas.vivPorPiso)
            << " viviendas\n";
    }


    return 0;
}
