#include <iostream>
#include <array>
#include <algorithm>    //  para std::short


template <typename T, std::size_t tamanio>  //  Para que el compilador deduzca el tipo y tamaño del array
void imprimirTam(const std::array<T, tamanio> &miArray){
    std::cout << "Tamaño: "<<miArray.size() << '\n';
}



int main(){

    std::array<int, 6> miArray {3, 5, 2, 9, 7, 2};

    imprimirTam(miArray);

    //std::sort(miArray.rbegin(), miArray.rend());      Ordenar el array descendentemente
    std::sort(miArray.begin(), miArray.end());         //Ordenar el array ascendentemente
    

    for(auto elementos : miArray){
        std::cout << elementos << ' ';
    }

    return 0;
}
