#include <iostream>
#include <array>
#include <iterator> //  Para usar std::begin y std::end

int main(){

    std::array datos{0, 1, 2, 3, 4, 5, 6};

    auto inicio{std::begin(datos)};  //auto inicio {&datos[0]};
    auto fin{std::end(datos)};  //auto fin {inicio + std::size(datos)};

    for(auto ptr {inicio}; ptr != fin; ptr++){
        std::cout << *ptr << ' ';
    }

    std::cout<<'\n';

    for (int i : datos)
        std::cout<<i<<' ';


    return 0;
}
