#ifndef BECADODEPORTE_HPP
#define BECADODEPORTE_HPP

#include "alumno.hpp"
#include "deportista.hpp"
#include <iostream>
using namespace std;

class BecadoDeporte : public Alumno, public Deportista {
    private:
        float CantidadBeca;

    public:
        BecadoDeporte(string nombreCarrera, string nombreEntrenador, float CantidadBeca);

        void mostrarDatos();
        void setCantidadBeca(float n);
        float getCantidadBeca();

        ~BecadoDeporte(){}
};


#endif