#include "deportista.hpp"
#include <iostream>

using namespace std;

Deportista::Deportista(string nombreEntrenador){
    this->nombreEntrenador = nombreEntrenador;
}


void
Deportista::mostrarDatos(){
    cout<<"Nombre Carrera: "<<nombreEntrenador<<endl;
}


void
Deportista::setNombreEntrenador(string nuevoNombre){
    this->nombreEntrenador = nuevoNombre;
}


string
Deportista::getNombreEntrenador(){
    return this->nombreEntrenador;
}