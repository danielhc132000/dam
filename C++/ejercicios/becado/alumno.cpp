#include "alumno.hpp"
#include <iostream>

using namespace std;

Alumno::Alumno(string nombreCarrera){
    this->nombreCarrera = nombreCarrera;
}


void
Alumno::mostrarDatos(){
    cout<<"Nombre Carrera: "<<nombreCarrera<<endl;
}


void
Alumno::setNombreCarrera(string nuevoNombre){
    this->nombreCarrera = nuevoNombre;
}


string
Alumno::getNombreCarrera(){
    return this->nombreCarrera;
}