#include "becadodeporte.hpp"
#include "alumno.hpp"
#include "deportista.hpp"
#include <iostream>
using namespace std;

BecadoDeporte::BecadoDeporte(string nombreCarrera, string nombreEntrenador, float CantidadBeca) : Alumno(nombreCarrera), Deportista(nombreEntrenador) {
    this->CantidadBeca = CantidadBeca;
}


void
BecadoDeporte::mostrarDatos(){
    Alumno::mostrarDatos();
    Deportista::mostrarDatos();
    cout<<"Cantidad Beca: "<<CantidadBeca<<endl;
}

void
BecadoDeporte::setCantidadBeca(float n){
    this->CantidadBeca += (this->CantidadBeca*n) / 100;
}


float
BecadoDeporte::getCantidadBeca(){
    return this->CantidadBeca;
}