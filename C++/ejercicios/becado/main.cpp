#include <iostream>
#include "alumno.hpp"
#include "deportista.hpp"
#include "becadodeporte.hpp"

using namespace std;

int main(int argc, char const *argv[]){

    Alumno *Juan = new Alumno("Programacion");
    Alumno *Pedro = new Alumno("Sistemas");
    Deportista *Daniel = new Deportista("Jaime");
    BecadoDeporte *Ana = new BecadoDeporte("Administracion", "Luis", 1200);
    BecadoDeporte *Clara = new BecadoDeporte("Contabilidad", "Jorge", 1430);

    cout<<"Mostrando Datos: \n";
    cout<<"\nMostrando datos de Juan: \n";
    Juan->mostrarDatos();
    cout<<"\nMostrando datos de Pedro: \n";
    Pedro->mostrarDatos();
    cout<<"\nMostrando datos de Daniel: \n";
    Daniel->mostrarDatos();
    cout<<"\nMostrando datos de Ana: \n";
    Ana->mostrarDatos();
    cout<<"\nMostrando datos de Clara: \n";
    Clara->mostrarDatos();

    string nuevaCarrera;
    cout<<"\nNueva carrera para Juan: ";
    cin>>nuevaCarrera;
    Juan->setNombreCarrera(nuevaCarrera);
    cout<<"Nueva carrera para Ana: ";
    cin>>nuevaCarrera;
    Ana->setNombreCarrera(nuevaCarrera);

    cout<<"La nueva carrera de Juan es: "<<Juan->getNombreCarrera()<<endl;
    cout<<"La nueva carrera de Ana es: "<<Ana->getNombreCarrera()<<endl;

    string nuevoEntrenador;
    cout<<"\nEl nuevo entrenador de Daniel es: "<<Daniel->getNombreEntrenador()<<endl;
    cout<<"Nuevo entrenador para Daniel: ";
    cin>>nuevoEntrenador;
    Daniel->setNombreEntrenador(nuevoEntrenador);

    cout<<"Nuevo entrenador para Clara: ";
    cin>>nuevoEntrenador;
    Clara->setNombreEntrenador(nuevoEntrenador);
    
    cout<<"\nEl nuevo entrenador de Daniel es: "<<Daniel->getNombreEntrenador()<<endl;
    cout<<"El nuevo entrenador de Clara es: "<<Clara->getNombreEntrenador()<<endl;

    float incremento;
    cout<<"Incremento de la beca de Ana: ";
    cin>>incremento;
    Ana->setCantidadBeca(incremento);
    cout<<"Incremento de la beca de Clara: ";
    cin>>incremento;
    Clara->setCantidadBeca(incremento);

    cout<<"La nueva beca de Ana es: "<<Ana->getCantidadBeca()<<endl;
    cout<<"La nueva beca de Clara es: "<<Clara->getCantidadBeca();

    delete Juan;
    delete Pedro;
    delete Daniel;

    return 0;
}
