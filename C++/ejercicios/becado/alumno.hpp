#ifndef ALUMNO_HPP
#define ALUMNO_HPP

#include <iostream>
using namespace std;

class Alumno {
    private:
        string nombreCarrera;

    public:
        Alumno(string nombreCarrera);

        void mostrarDatos();
        void setNombreCarrera(string nuevoNombre);
        string getNombreCarrera();

        ~Alumno(){}
};


#endif