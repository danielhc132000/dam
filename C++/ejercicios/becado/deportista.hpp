#ifndef DEPORTISTA_HPP
#define DEPORTISTA_HPP

#include <iostream>
using namespace std;

class Deportista {
    private:
        string nombreEntrenador;

    public:
        Deportista(string nombreEntrenador);

        void mostrarDatos();
        void setNombreEntrenador(string nuevoNombre);
        string getNombreEntrenador();

        ~Deportista(){}
};

#endif