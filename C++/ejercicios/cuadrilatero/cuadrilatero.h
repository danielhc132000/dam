#ifndef __CUADRILATERO_H__
#define __CUADRILATERO_H__

class Cuadrilatero {

    friend int main();

    private:
        double base, altura;

    public:
        Cuadrilatero(double);
        Cuadrilatero(double, double);

        double area();
        double perimetro();

};

#endif