#include <iostream>

#include "cuadrilatero.h"

Cuadrilatero::Cuadrilatero(double altura) : altura(altura), base(0) {}
Cuadrilatero::Cuadrilatero(double base, double altura) : base(base), altura(altura) {}


double
Cuadrilatero::area(){
    if(this->base==0)
        return altura * altura;
    else
        return base * altura;
}

double
Cuadrilatero::perimetro(){
    if(this->base==0)
        return altura * 4;
    else
        return base * 2 + altura * 2;
}


