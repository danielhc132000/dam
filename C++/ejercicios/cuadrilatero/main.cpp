#include <iostream>

#include "cuadrilatero.h"


int main(){

    Cuadrilatero c1(12);
    Cuadrilatero c2(18, 12);

    std::cout<<"El area es: "<<c1.area()<<"\n";
    std::cout<<"El perimetro es: "<<c1.perimetro()<<"\n";

    std::cout<<"El area es: "<<c2.area()<<"\n";
    std::cout<<"El perimetro es: "<<c2.perimetro()<<"\n";

    return 0;

}