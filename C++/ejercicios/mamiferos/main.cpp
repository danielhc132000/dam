#include <iostream>
#include "gatoDomestico.hpp"
#include "felino.hpp"

using namespace std;

int main(){

    GatoDomestico *gato = new GatoDomestico(2017, "Madrid", "Gato Persa", "Daniel Hernandez");
    Felino *tigre = new Felino(2016, "La India", "Tigre de Vengala", "Circular");

    cout<<"Dietas:\n";
    cout<<"Gato: "<<gato->imprimirDieta()<<endl;
    cout<<"Felino: "<<tigre->imprimirDieta();

    cout<<"\nAño Nacimiento - Lugar Nacimiento: \n";
    cout<<"Gato: "<<gato->getFechaNacimiento()<<" - "<<gato->getLugarNacimiento()<<endl;
    cout<<"Felino: "<<tigre->getFechaNacimiento()<<" - "<<tigre->getLugarNacimiento()<<endl;

    cout<<"\nCambiar nombre del dueño del Gato: \n";
    string nuevoduenio;
    cout<<"Nuevo Dueño: ";
    cin>>nuevoduenio;
    gato->setNombreDuenio(nuevoduenio);
    cout<<"EL nuevo dueño es: "<<gato->getNombreDuenio()<<endl;

    cout<<"\nRaza: \n";
    cout<<"Gato: "<<gato->getRaza()<<endl;
    cout<<"Felino: "<<tigre->getRaza()<<endl;

    cout<<"\nCambiar nombre del circo: \n";
    string nuevoCirco;
    cout<<"Nombre del nuevo circo: ";
    cin>>nuevoCirco;
    tigre->setNombreCirco(nuevoCirco);
    cout<<"El nuevo circo es: "<<tigre->getNombreCirco()<<endl;



    return 0;
}