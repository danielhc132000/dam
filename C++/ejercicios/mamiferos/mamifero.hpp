#ifndef MAMIFERO_HPP
#define MAMIFERO_HPP

#include <string>

using namespace std;

class Mamifero {
    private:
        int fechaNacimiento;
        string lugarNacimiento;
        string raza;

    public:
        Mamifero(int fechaNacimiento, string lugarNacimiento, string raza);
        virtual string imprimirDieta() = 0;

        int getFechaNacimiento();
        string getLugarNacimiento();
        string getRaza();
};


#endif