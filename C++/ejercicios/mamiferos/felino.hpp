#ifndef FELINO_HPP
#define FELINO_HPP

#include <iostream>
#include "mamifero.hpp"

using namespace std;

class Felino : public Mamifero{
    private:
        string nombreCirco;

    public:
        Felino(int fechaNacimiento, string lugarNacimiento, string raza, string nombreCirco);
        Felino(int fechaNacimiento, string lugarNacimiento, string raza);
        ~Felino(){}

        string imprimirDieta();

        void setNombreCirco(string nuevoNombre);
        string getNombreCirco();
};

#endif