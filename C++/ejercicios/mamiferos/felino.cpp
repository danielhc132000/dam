#include <iostream>
#include "felino.hpp"



Felino::Felino(int fechaNacimiento, string lugarNacimiento, string raza, string nombreCirco) : Mamifero(fechaNacimiento, lugarNacimiento, raza){
    this->nombreCirco = nombreCirco;
}

Felino::Felino(int fechaNacimiento, string lugarNacimiento, string raza) : Mamifero(fechaNacimiento, lugarNacimiento, raza){}

string
Felino::imprimirDieta(){
    return "La dieta del felino es a base de carne\n";
}

void 
Felino::setNombreCirco(string nuevoNombre){
    this->nombreCirco = nuevoNombre;
}

string
Felino::getNombreCirco(){
    return this->nombreCirco;
}