#include "mamifero.hpp"
#include <iostream>

using namespace std;


Mamifero::Mamifero(int fechaNacimiento, string lugarNacimiento, string raza){
    this->fechaNacimiento = fechaNacimiento;
    this->lugarNacimiento = lugarNacimiento;
    this->raza = raza;
}


int
Mamifero::getFechaNacimiento(){
    return this->fechaNacimiento;
}



string
Mamifero::getLugarNacimiento(){
    return this->lugarNacimiento;
}



string
Mamifero::getRaza(){
    return this->raza;
}