#ifndef GATODOMESTICO_HPP
#define GATODOMESTICO_HPP

#include "felino.hpp"
#include <iostream>

class GatoDomestico : public Felino {
    private:
        string nombreDuenio;

    public:
        GatoDomestico(int fechaNacimiento, string lugarNacimiento, string raza, string nombreDuenio);
        ~GatoDomestico(){}

        string imprimirDieta();
        void setNombreDuenio(string nuevoDuenio);
        string getNombreDuenio();
};


#endif