#include <iostream>
#include "felino.hpp"
#include "gatoDomestico.hpp"

using namespace std;

GatoDomestico::GatoDomestico(int fechaNacimiento, string lugarNacimiento, string raza, string nombreDuenio) : Felino(fechaNacimiento, lugarNacimiento, raza){
    this->nombreDuenio = nombreDuenio;
}

string
GatoDomestico::imprimirDieta(){
    return "La dieta del gato es pienso.";
}

void
GatoDomestico::setNombreDuenio(string nuevoDuenio){
    this->nombreDuenio = nuevoDuenio;
}

string
GatoDomestico::getNombreDuenio(){
    return this->nombreDuenio;
}