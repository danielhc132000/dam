/* Los comentarios multilinea indican otra opcion de codigo */

#include <iostream>
#include <stdlib.h>
#include "triangle.h"

using namespace std;


void title(){
    system("clear");
    system("toilet -f smblock --filter border:metal 'Teorema de Pitagoras'");
    cout<<"\n\n";
}

void
ask_user(Triangle *c){  /* void ask_user(Lado &c) {} */
    static int cateto=1;
    cout<<"Medida del "<<cateto<<"º cateto: ";
    cin>>c->side;   /* cin>>c.side; */
    cateto++;
}


int
main(){

    Triangle c1, c2, h;

    title();
    ask_user(&c1);  /* ask_user(c1) */
    ask_user(&c2);  /* ask_user(c2) */

    h = c1 + c2;    //  Los operator se aplican directamente al objeto de una clase y no a sus atributos

    cout<<"\nLa medida de la hipotenusa es: "<<h.side;


    return EXIT_SUCCESS;
}

//  Pasar un una direccion y almacenarlo en un puntero como parametro es lo mismo que pasar el valor
//  de una variable y recibir la direccion del mismo como parametro de una funcion que son las dos
//  opciones de codigo
