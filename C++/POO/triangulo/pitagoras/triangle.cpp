#include "triangle.h"
#include <cmath>

Triangle::Triangle(double lado) : side(lado) {}

double Triangle::operator+ (Triangle &lado) const{
    return sqrt(pow(this->side, 2)+pow(lado.side, 2));
}
