#include <iostream>

#include "mates.h"

int main(){

    Triangulo c1, c2, result;
    double resultado;

    c1.setCateto(10);
    c2.setCateto(5);

    /*1.*/result.setCateto(c1 + c2);
    /*2.*/resultado = c1 + c2;

    std::cout<<"La hipotenusa de los dos catetos es: "<<result.getCateto();
    //std::cout<<"La hipotenusa de los dos catetos es: "<<resultado;

    return 0;
}