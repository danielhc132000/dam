
class Triangulo {

    public:
        //  Constructores
        Triangulo();
        
        double operator+ (Triangulo &lado);    
        void setCateto(double);
        double getCateto();

    private:
        double cateto;

};