#include <cmath>
#include "mates.h"


Triangulo::Triangulo(){}

double Triangulo::getCateto(){
    return this->cateto;
}

void Triangulo::setCateto(double cateto){
    this->cateto = cateto;
}

double Triangulo::operator+ (Triangulo &lado){
    return sqrt(pow(this->getCateto(), 2)+pow(lado.getCateto(), 2));
}
