#ifndef __VECTOR_H__
#define __VECTOR_H__

class Vector {

    public:

    double x, y, z;   // Poner los atributos públicos no suele ser muy buena idea. Valga como ejemplo.


    Vector (double x=0, double y=0, double z=0);    // En caso de no recivir parametros valdran 0

    Vector operator+ (const Vector &op);    //  Suma el valor de this y el valor del que reciva el parametro

};

#endif
