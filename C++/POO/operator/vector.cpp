#include "vector.h"

Vector::Vector (double x, double y, double z):  //  Se le da a los atributos de la clase el valor de los parametros
    x(x), y(y), z(z)
{}

Vector Vector::operator+ (const Vector &op) {  //   Suma el valor de this "operator" mas el parametro y le retorna

    return Vector ( //  Retorna un vector con la suma del primer vector en operator "this" y el segundo en el parametro
            this->x + op.x,
            this->y + op.y,
            this->z + op.z
            );
}
