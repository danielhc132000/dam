#include <stdio.h>
#include <stdlib.h>

#include "vector.h"

void imprimir (Vector v) {
    printf ("(%.2lf, %.2lf, %.2lf)", v.x, v.y, v.z);
}

int main (int argc, char *argv[]) {

    Vector v0 = Vector (2, 3, 5);   //  Crea el Vector v0 y le da los valores con un constructor
    Vector v1(5, 2, 3); //  Directamente crea el Vector  v1 con los valores indicados
    Vector resul = v0 + v1; //  Crea el Vector result que es la suma de los anteriores dos vectores, el constructor retorna los valores de la suma de los vectores y los guarda en el otro vector resul

    imprimir (v0);  //  Funcion que imprime el primer vector
    printf (" + "); 
    imprimir (v1);  //  Imprime el segundo vector
    printf (" = ");
    imprimir (resul);   //  Imprime el resultado
    printf ("\n");  

    return EXIT_SUCCESS;
}
