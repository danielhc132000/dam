#ifndef GETSET_H
#define GETSET_H


class Getset {
    private:
        int x;
        int y;

    public:
        Getset();
        Getset(int, int);

        int getX();
        int getY();
        void setX(int);
        void setY(int);
};

#endif
