
#include "getset.h"

Getset::Getset () : x(0), y(0) {}
Getset::Getset (int _x, int _y) : x(_x), y(_y) {}


int Getset::getX () { return x; }
int Getset::getY () { return y; }

void Getset::setX (int _x) { x = _x; }
void Getset::setY (int _y) { y = _y; }
