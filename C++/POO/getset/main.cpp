#include <iostream>
#include <stdlib.h>

#include "getset.h"

int
main(int argc, char **argv){

    int x, y;
    Getset p1(5, 10);
    
    std::cout<<"El valor de x: "<<p1.getX();
    std::cout<<"\nEl valor de x: "<<p1.getY();

    Getset p2;
    
    std::cout<<"\n\nValor de x: ";
    std::cin>>x;
    p2.setX(x);

    std::cout<<"Valor de y: ";
    std::cin>>y;
    p2.setY(y);
    

    std::cout<<"\nEl valor de x: "<<p2.getX();
    std::cout<<"\nEl valor de x: "<<p2.getY();



    return EXIT_SUCCESS;
}
