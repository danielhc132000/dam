

class Estatico {

    private:
        static int contador;    //  Declaracion de un atributo static
        //inline static int contador = 0;
        int posicion;
    public:
        Estatico();

        int getPosicion();
        static int sumar (int n1, int n2);
};
/*Los atributos static pueden ser declarados, y darles un valor definiendolo
en el .cpp, o podemos declararlo con "inline" y asignarle un valor en el .h, con
"inline" definira al comientzo del .cpp el atributo estatico
*/