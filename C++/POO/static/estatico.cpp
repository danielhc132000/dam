#include "estatico.h"

int Estatico::contador = 0;

Estatico::Estatico(){
    this->posicion = Estatico::contador++;
}


int
Estatico::getPosicion(){ return posicion; }


int
Estatico::sumar(int n1, int n2){
    return n1 + n2;
}