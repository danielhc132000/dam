#include <iostream>
#include "estatico.h"

#define N 5


int main(){

    Estatico n[N];

    for(int i=0; i<N; i++)
        std::cout<<"Posicion de los objetos: "<<n[i].getPosicion()<<"\n";

    std::cout<<"La suma es: "<<Estatico::sumar(4, 5);

    return 0;
}