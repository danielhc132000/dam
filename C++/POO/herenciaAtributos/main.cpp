#include <iostream>
#include "clasederivada.hpp"

using namespace std;

int main(int argc, char const *argv[]){
    
    ClaseDerivada c1, c2;

    c1.dar();
    cout<<"Numero de c1: "; c1.ver();

    c2.dar();
    cout<<"Numero de c2: "; c2.ver();

    cout<<"Numero de c1 tras dar valor a c2: "; c1.ver();

    return 0;
}
