#include "clasebase.hpp"


class ClaseDerivada : public ClaseBase {
    public:
        void dar();
        void ver();
};

/*
    Cuando se hereda una clase base a parte de heredar los metodos tambien heredaremos los atributos que seran una copia de los
    que tiene la clase padre y seran independientes entre ellos y entre los demas objetos de las clases hijas, es decir, diferentes
    atributos en cada objeto pero iguales a los de la clase padre
*/