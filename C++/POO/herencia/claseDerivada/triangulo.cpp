#include <cmath>

#include "triangulo.h"


Triangulo::Triangulo(int nLados, float l1, float l2, float l3) : Figura(nLados){
    this->lado1 = l1;
    this->lado2 = l2;
    this->lado3 = l3;
}

float
Triangulo::areaTriangulo(){
    float p = (lado1+lado2+lado3) / 2;
    float area = sqrt(p*(p-lado1)*(p-lado2)*(p-lado3));
    return area;
}