#include <iostream>

#include "triangulo.h"

int main(int argc, char const *argv[])
{
    
    Triangulo t1(3,4,6,7);

    std::cout<<"Numero de lados: "<<t1.getNLados()<<"\n";
    std::cout<<"Area: "<<t1.areaTriangulo();


    return 0;
}
