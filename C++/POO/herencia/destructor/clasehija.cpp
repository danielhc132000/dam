#include <iostream>
#include "clasehija.h"

ClaseHija::ClaseHija(int numero, int numero2) : ClaseBase(numero) {
    this->numero2 = numero2;
    std::cout<<"Constructor de la clase hija.\n";
}


ClaseHija::~ClaseHija(){
    std::cout<<"Destructor de la clase hija\n";
}
