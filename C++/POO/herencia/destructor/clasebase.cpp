#include <iostream>

#include "clasebase.h"


ClaseBase::ClaseBase(int numero){
    this->numero = numero;
    std::cout<<"Constructor de la clase base.\n";
}


ClaseBase::~ClaseBase(){
    std::cout<<"Destructor de la clase base.\n";
}
