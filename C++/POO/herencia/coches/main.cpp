#include <iostream>
#include <stdlib.h>

#include "turismo.h"
#include "deportivo.h"
#include "furgoneta.h"

using namespace std;

int main(){

    cout<<"---Herencia Publica---\n\n";
    Turismo *t1 = new Turismo("Toyota", "Blanco", "GH89", 4);

    cout<<"Color: "<<t1->getColor()<<"\n";   //  Atributo de la clase Vehiculo
    
    t1->setColor("Negro"); 
    cout<<"Color: "<<t1->getColor()<<"\n";
   
    cout<<"Modelo: "<<t1->returnModelo();

    cout<<"\n\n---Herencia Privada---\n\n";

    Deportivo *d1 = new Deportivo("Audi", "Blanco", "R8", 250);

    cout<<"Caballos: "<<d1->getCaballos()<<"\n";
    cout<<"Marca: "<<d1->returnMarca()<<"\n";

    cout<<"\n\n---Herencia Protegida---\n\n";
    
      Furgoneta *f1 = new Furgoneta("Renault", "Gris", "R4", 1200);

    cout<<"Carga: "<<f1->getCarga()<<"\n";
    cout<<"Color: "<<f1->returnColor();

    delete t1;
    delete d1;
    delete f1;

    return EXIT_SUCCESS;
}


