#include <iostream>
#include "furgoneta.h"


using namespace std;

Furgoneta::Furgoneta(string marca, string color, string modelo, int carga) : Vehiculo(marca, color, modelo){
    this->carga = carga;
}

int
Furgoneta::getCarga(){
    return this->carga;
}


string
Furgoneta::returnColor(){
    return getColor();
}
