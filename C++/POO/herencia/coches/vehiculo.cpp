#include <iostream>
#include <stdlib.h>

#include "vehiculo.h"

using namespace std;


Vehiculo::Vehiculo(string marca, string color, string modelo){
    this->marca = marca;
    this->color = color;
    this->modelo = modelo;
}

string
Vehiculo::getModelo(){
    return this->modelo;
}

string
Vehiculo::getMarca(){
    return this->marca;
}

string
Vehiculo::getColor(){
    return this->color;
}

void
Vehiculo::setColor(string color){
    this->color = color;
}
