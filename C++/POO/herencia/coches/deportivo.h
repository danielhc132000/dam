#include <iostream>
#include "vehiculo.h"

using namespace std;

class Deportivo : private Vehiculo {

    private:
        int caballos;

    public:
        Deportivo(string marca, string color, string modelo, int caballos);
        int getCaballos();
        string returnMarca();

};
