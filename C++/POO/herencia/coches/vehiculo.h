#ifndef VEHICULO_H
#define VEHICULO_H

#include <iostream>
using namespace std;

class Vehiculo {

    private:
        string marca;
        string color;

    protected:
        string modelo;
        string getModelo();
    public:
        Vehiculo(string, string, string);
        string getMarca();
        string getColor();

        void setColor(string);

};

#endif
