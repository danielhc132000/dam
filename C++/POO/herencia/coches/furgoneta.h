#include <iostream>
#include "vehiculo.h"

using namespace std;


class Furgoneta : protected Vehiculo{

    private:
        int carga;

    public:
        Furgoneta(string marca, string color, string modelo, int carga);
        int getCarga();
        string returnColor();
};
