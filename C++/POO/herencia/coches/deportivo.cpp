#include <iostream>
#include "deportivo.h"

using namespace std;


Deportivo::Deportivo(string marca, string color, string modelo, int caballos) : Vehiculo(marca, color, modelo){
    this->caballos = caballos;
}


int
Deportivo::getCaballos(){
    return this->caballos;
}


string
Deportivo::returnMarca(){
    return getMarca();
}
