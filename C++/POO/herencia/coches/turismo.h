#include <iostream>
#include "vehiculo.h"

using namespace std;

class Turismo : public Vehiculo {

    private:
        int numeroPuertas;

    public:
        Turismo(string marca, string color, string modelo, int numeroPuertas);
        int getNumeroPuertas();
        ~Turismo();

        string returnModelo();

};
