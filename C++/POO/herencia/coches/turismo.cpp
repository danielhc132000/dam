#include <iostream>
#include <stdlib.h>

#include "turismo.h"


Turismo::Turismo(string marca, string color, string modelo, int numeroPuertas) : Vehiculo(marca, color, modelo){
    this->numeroPuertas = numeroPuertas;
}


int Turismo::getNumeroPuertas(){
    return this->numeroPuertas;
}


string
Turismo::returnModelo(){
    return getModelo();
}


Turismo::~Turismo(){}
