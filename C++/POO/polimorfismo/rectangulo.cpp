#include <iostream>
#include "rectangulo.hpp"

using namespace std;

Rectangulo::Rectangulo(float base, float altura){
	this->base = base;
	this->altura = altura;
}

float
Rectangulo::perimetro(){
	return 2 * base + 2 * altura;
}


float
Rectangulo::area(){
	return base * altura;
}