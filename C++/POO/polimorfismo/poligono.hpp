#pragma once

class Poligono {
	public:
		virtual float perimetro() = 0;
		virtual float area() = 0;
};