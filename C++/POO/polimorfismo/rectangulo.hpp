#pragma once
#include "poligono.hpp"


class Rectangulo : public Poligono {
	private:
		float base, altura;

	public:
		Rectangulo(float, float);
		float perimetro();
		float area();
};