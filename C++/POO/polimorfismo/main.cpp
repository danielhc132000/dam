#include <iostream>

#include "poligono.hpp"
#include "rectangulo.hpp"
#include "triangulo.hpp"

using namespace std;

int main(int argc, char const *argv[]){
	
	Poligono *p[2];

	p[0] = new Rectangulo(7,4);
	p[1] = new Triangulo(3,4,4);


	for(int i=0; i<2; i++){
		cout<<"Area: "<<p[i]->area()<<"\n";
		cout<<"Perimetro: "<<p[i]->perimetro()<<"\n\n";
	}


	return 0;
}