#include <iostream>

#include "op.h"


void Op::run(){
    this->preguntar(&n1, &n2);  //En la direccion de Op, mandaremos al metodo preguntar direcciones de variables

    this->suma(this->n1, this->n2); // En Op mandaremos al metodo suma el valor de n1 y n2
}

void Op::preguntar(int *n1, int *n2){

    std::cout<<"Primer numero: ";   //  Guardamos los valores alli donde apuntan los parametros
    std::cin>>*n1;

    std::cout<<"Segundo numero: ";
    std::cin>>*n2;

}


void Op::suma(int n1, int n2) { 
    this->total = n1+n2;    // Guardamos en la variable total de Op la suma de las dos variables

    std::cout<<"La suma de los dos numeros es: "<<this->total;

}
