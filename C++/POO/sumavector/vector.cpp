#include "vector.h"


Vector::Vector(double x, double y) : x(x), y(y) {}


Vector Vector::operator+ (Vector &vec) const{
    
    return Vector (
            this->x+vec.x,
            this->y+vec.y
            );
}
