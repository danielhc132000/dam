#include <iostream>

#include "vector.h"


void
ask_user(Vector *v){
    
    static unsigned i=1;
    std::cout<<"\nVector "<<i<<" eje X: ";
    std::cin>>v->x; 
    
    std::cout<<"Vector "<<i<<" eje Y: ";
    std::cin>>v->y; 

    i++;
}


void
final_vector(Vector v1, Vector v2, Vector v3, Vector resultado){
    
    system("clear");
    printf ("\n (%.3lf, %.3lf)\n+(%.3lf, %.3lf)\n (%.3lf, %.3lf)",
            v1.x, v1.y, v2.x, v2.y, v3.x, v3.y);

    printf("\n\n(%.3lf, %.3lf)", resultado.x, resultado.y);

}


int main (){

    Vector v1, v2, v3, resultado;

    ask_user(&v1);
    ask_user(&v2);
    ask_user(&v3);

    resultado = v1 + v2 + v3;

    final_vector(v1, v2, v3, resultado);

    return 0;

}
