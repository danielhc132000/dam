#ifndef VECTOR_H
#define VECTOR_H


class Vector{

    public:
        Vector(double x=0, double y=0);
        
        Vector operator+ (Vector &vec) const;

        double x;
        double y;

};


#endif
