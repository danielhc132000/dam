#include <string>
using namespace std;

class Avion{
    private:
        string modelo;

    public:
        Avion(string modelo);
        ~Avion();
        void indicarAvion();
        string getModelo();

};
