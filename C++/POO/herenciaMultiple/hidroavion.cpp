#include <iostream>
#include "hidroavion.h"

using namespace std;

HidroAvion::HidroAvion(string nombre, string modelo, string codigo) : Barco(nombre), Avion(modelo) {
    this->codigo = codigo;
};


string
HidroAvion::getCodigo(){
    return this->codigo;
}


void
HidroAvion::mostrarDatos(){
    cout<<"Nombre: "<<getNombre()<<"\n";
    cout<<"Modelo: "<<getModelo()<<"\n";
    cout<<"Codigo: "<<codigo;
}
