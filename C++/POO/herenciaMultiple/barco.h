#include <string>
using namespace std;

class Barco {

    private:
        string nombre;

    public:
        Barco(string nombre);
        ~Barco();
        void indicarBarco();
        string getNombre();
};
