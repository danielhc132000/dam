#include <iostream>
#include "barco.h"

using namespace std;


Barco::Barco(string nombre){
    this->nombre = nombre;
}

Barco::~Barco(){}


void
Barco::indicarBarco(){
    cout<<"Desplazamiento por agua.\n";
}


string
Barco::getNombre(){
    return this->nombre;
}
