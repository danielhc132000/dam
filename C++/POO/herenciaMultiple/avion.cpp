#include <iostream>
#include "avion.h"

using namespace std;

Avion::Avion(string modelo){
    this->modelo = modelo;
}

Avion::~Avion(){}


void
Avion::indicarAvion(){
    cout<<"Desplazamiento por aire";
}


string
Avion::getModelo(){
    return this->modelo;
}
