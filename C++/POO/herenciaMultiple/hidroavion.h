#include <string>

#include "barco.h"
#include "avion.h"

using namespace std;


class HidroAvion : public Barco, public Avion {

    private:
        string codigo;

    public:
        HidroAvion(string nombre, string modelo, string codigo);
        ~HidroAvion(){}
        string getCodigo();
        void mostrarDatos();

};
