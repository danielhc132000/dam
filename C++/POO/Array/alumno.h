#ifndef ALUMNO_H
#define ALUMNO_H

class Alumno {

    private:
        float calMates, calProgram, media;

    public:
        void pedirDatos();
        void verNotas();

};

#endif
