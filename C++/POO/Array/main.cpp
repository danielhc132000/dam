#include <iostream>
#include <stdlib.h>

#include "alumno.h"

int
main(int argc, char **argv){

    Alumno alumnos[3];  //  Array de objetos estatico

    Alumno *alumnos2 = new Alumno[3];   //  Array de objetos dinamicos


    for(int i=0; i<3; i++){
        (alumnos2+i)->pedirDatos();
        //alumnos[i].pedirDatos();
    }

    std::cout<<"\nMostrando las notas: \n\n";

    for(int i=0; i<3; i++){
        (alumnos2+i)->verNotas();
        //alumnos[i].pedirDatos();
    }

    return EXIT_SUCCESS;
}

/*  Se usa un constructor por defecto porque al crear un array de objetos no se les da valores a los mismos*/
