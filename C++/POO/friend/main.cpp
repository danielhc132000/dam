#include <iostream>

#include "personaje.h"


void modificar(Personaje &p, int at, int def){
    p.defensa = def;    
    p.ataque = at;
}


int main(){

    Personaje principal(50, 50);

    principal.mostrarDatos();

    modificar(principal, 120, 100);

    principal.mostrarDatos();

    return 0;
}