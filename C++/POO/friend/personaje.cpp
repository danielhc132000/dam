#include <iostream>

#include "personaje.h"


Personaje::Personaje(int ataque, int defensa) : ataque(ataque), defensa(defensa) {}

void
Personaje::mostrarDatos(){

    std::cout<<"Ataque = "<<this->ataque<<"\n";
    std::cout<<"Defensa = "<<this->defensa<<"\n\n";
}