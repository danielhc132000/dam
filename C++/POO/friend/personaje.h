

class Personaje {
    friend void modificar(Personaje &, int, int);

    private:
        int ataque;
        int defensa;

    public:
        Personaje(int, int);
        void mostrarDatos();
};