#pragma once
#include "animal.hpp"

class AnimalCarnivoro : public Animal {
    public:
        void alimentarse();
};