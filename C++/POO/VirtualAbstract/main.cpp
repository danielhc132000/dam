#include <iostream>
#include "planta.hpp"
#include "animalcarnivoro.hpp"
#include "animalhervivoro.hpp"

int main(int argc, char const *argv[])
{
    Planta *planta1 = new Planta();
    AnimalCarnivoro *animmal1 = new AnimalCarnivoro();
    AnimalHervivoro *animal2 = new AnimalHervivoro();

    planta1->alimentarse();
    animmal1->alimentarse();
    animal2->alimentarse();

    
    return 0;
}