#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX 10

int
main ()
{
  float num;

  for (num = 0; num < MAX; num++)
    printf ("%09.2f\n", num);

  return 0;
}
