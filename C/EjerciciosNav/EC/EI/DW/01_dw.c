#include <stdio.h>
#include <stdlib.h>
#include <string.h>



int
main ()
{

  int num;

  do
    {

      printf ("Número '1-10': ");
      scanf ("%i", &num);

      if (num >= 1 && num <= 10)
	{			
	  printf ("Tu número es: %i\n", num);
	}
      else{
	  printf ("Numero incorrecto.\n");
	}
    }
  while (num > 10 || num < 1);

  return EXIT_SUCCESS;
}
