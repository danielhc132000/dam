#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX 30

int
main ()
{

  char frase[MAX];
  int c=0;

  printf ("Escribe una frase: ");
  fgets (frase, MAX, stdin);

  int longfrase = strlen (frase);

  while (c < longfrase)
    {
      printf ("%c", frase[c]);
      c++;
    }

  return 0;
}
