#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX 30

int
main ()
{

  char frase[MAX];
  int longfrase;

  printf ("Escribe una frase");
  fgets (frase, MAX, stdin);

  longfrase = strlen (frase);

  while (longfrase != '\0' - 1)
    {
      printf ("%c", frase[longfrase]);
      longfrase--;
    }

  printf ("\n");

  return 0;
}
