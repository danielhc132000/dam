#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int
main ()
{

char letra;
int n;

printf("Caracter a imprimir: ");
scanf("%c", &letra);

printf("Veces a repetir: ");
scanf("%i", &n);

for(int i=0; i<n; i++)
	printf("%c ", letra);
printf("\n");
  return 0;
}
