#include <stdio.h>
#include <stdlib.h>
#include <stdio_ext.h>
#include <string.h>
#include <cstring>

  int main() {

      int opcion;

      printf("Bienvenido, por favor seleccione una operación:\n");	
      printf("1) Listar un directorio a elección. \n");
      printf("2) Ver los que ocupa un directorio y un subdirectorio. \n");
      printf("3) Consultar la cantidad de espacio libre que hay en cada disco. \n");
      printf("4) Mostrar la ocupación de memoria. \n \n");      

      printf("Por favor escoje la operación con un número del 1 al 4: ");
      scanf("%i", &opcion);

      switch (opcion){

      case 1:{
      
      __fpurge(stdin);	      
      
      char directorio[20];
      char comando[20] = "ls -lh ";
      
      printf("Escriba el directorio a listar o si prefiere el directorio actual escribe '0': ");
      scanf("%s", directorio);

      if(directorio[0] == '0') {
      system(comando);
}
         else{
      strcat(comando, directorio);
      system(comando);

} 
}
     
      break;
      
      case 2:
      system("du -sh");
 
      break;

      case 3:
      system("df -h");

      break;

      case 4:
      system("free");

      break;

      default:
      printf("Opción incorrecta\n");
      	
}
      return 0;
}
