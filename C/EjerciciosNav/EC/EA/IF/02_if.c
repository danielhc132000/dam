#include <stdio.h>
#include <stdlib.h>

int
main ()
{

  char resp;

  printf ("¿Quieres instalar algo? Responde con 's(si)/n(no)': ");
  scanf ("%c", &resp);

  if (resp == 's')
    {
      printf ("Pues lo instalo\n");
    }
  if(resp == 'n')
    {
      printf ("Pues no lo instalo\n");
    }

  return EXIT_SUCCESS;
}
