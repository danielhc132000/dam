#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

int
main ()
{

  char resp;

  printf ("¿Quieres instalar algo? Responde con 's(si)/n(no)': ");
  scanf ("%c", &resp);

  if (tolower (resp) == 's')
    {
      printf ("Pues lo instalo\n");
    }
  else
    {
      printf ("Pues no lo instalo\n");
    }
  return 0;
}
