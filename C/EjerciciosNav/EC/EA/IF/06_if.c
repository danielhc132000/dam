#include <stdio.h>
#include <stdlib.h>

#define E 0.001

int
main ()
{

  float num;

  printf ("Numero: ");
  scanf ("%f", &num);

  if (num >= 3 - E && num <= 3 + E)
    {
      printf ("Se encuentra dentro del entorno\n");
    }
  else
    {
      printf ("Esta fuera del entorno\n");
    }

  return EXIT_SUCCESS;
}
