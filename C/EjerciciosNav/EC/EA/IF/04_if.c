#include <stdio.h>
#include <stdlib.h>
#include <stdio_ext.h>

int
main ()
{

  char tos;
  char maullar;

  printf ("¿Tienes tos? Responde con 's(si)/n(no)': ");
  scanf ("%c", &tos);

  __fpurge (stdin);

  printf ("¿Y maúllas? Responde con 's(si)/n(no)': ");
  scanf ("%c", &maullar);

  if (tos == 'n')
    {
      if (maullar == 'n')
	{
	  printf ("Estasa sano\n");
	}
      else
	{
	  printf ("Eres un gato\n");
	}
    }
  else
    {
      if (maullar == 'n')
	{
	  printf ("Tienes tos ferina\n");
	}
      else
	{
	  printf ("Tienes tos felina\n");
	}
    }

  return 0;
}
