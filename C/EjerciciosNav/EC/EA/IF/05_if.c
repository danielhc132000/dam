#include <stdio.h>
#include <stdlib.h>
#include <stdio_ext.h>

int
main ()
{

  int rojo;
  int verde;
  int azul;

  printf ("PIENSA EN UN COLOR POR FAVOR. \n");

  printf ("¿Contiene el rojo? Responde con '1(si)/0(no)': ");
  scanf ("%i", &rojo);

  __fpurge (stdin);

  printf ("¿Contiene el verde? Responde con '1(si)/0(no)': ");
  scanf ("%i", &verde);

  __fpurge (stdin);

  printf ("¿Contiene el azul? Responde con '1(si)/0(no)': ");
  scanf ("%i", &azul);

  if (rojo == 0 && verde == 0 && azul == 0)
    {
      printf ("Es negro\n");
    }
  else if (rojo == 1 && verde == 1 && azul == 1)
    {
      printf ("Es blanco\n");
    }
  else if (rojo == 1 && verde == 0 && azul == 0)
    {
      printf ("Es rojo\n");
    }
  else if (rojo == 1 && verde == 1 && azul == 0)
    {
      printf ("Es marrón\n");
    }
  else if (rojo == 1 && verde == 0 && azul == 1)
    {
      printf ("Es morado\n");
    }
  else if (rojo == 0 && verde == 1 && azul == 0)
    {
      printf ("Es verde\n");
    }
  else if (rojo == 0 && verde == 1 && azul == 1)
    {
      printf ("Es turquesa\n");
    }
  else if (rojo == 0 && verde == 0 && azul == 1)
    {
      printf ("Es azul\n");
    }

  return 0;
}
