#include <stdio.h>
#include <stdlib.h>

int
main ()
{
  int num;
  printf ("Número: ");
  scanf ("%i", &num);

  if (num % 2 == 0)
    {
      printf ("%i es par.\n", num);
    }
  else
    {  
      printf ("%i es impar.\n", num);
    }
  return 0;
}
