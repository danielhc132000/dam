#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void
pedir_entero (int *var)
{

  printf ("Numero entero: ");
  scanf ("%i", var);

}

int
main ()
{
  int num;

  pedir_entero (&num);

  printf ("El numero escrito es: %i", num);

  return 0;
}
