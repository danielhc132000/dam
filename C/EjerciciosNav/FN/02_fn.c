#include <stdio.h>
#include <stdlib.h>
#include <string>

int
suma (int n1, int n2)
{

  return n1 + n2;

}

int
main ()
{

  int n1, n2;

  printf ("Primer numero: ");
  scanf ("%i", &n1);
  printf ("Segundo numero: ");
  scanf ("%i", &n2);

  printf ("Resultado: %i\n", suma (n1, n2));

  return EXIT_SUCCESS;
}
