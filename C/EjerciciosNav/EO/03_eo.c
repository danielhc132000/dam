#include <stdio.h>
#include <stdlib.h>


int
main ()
{

  int a;
  long int b;
  long long int c;

  printf ("int = %li\n", sizeof (a));
  printf ("long int =  %li\n", sizeof (b));
  printf ("long long int =  %li\n", sizeof (c));

  double d;
  long double e;

  printf ("double = %ld\n", sizeof (d));
  printf ("long double = %ld\n", sizeof (e));


  return EXIT_SUCCESS;

}
