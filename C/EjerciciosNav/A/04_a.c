#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX 10

int
main ()
{

  int nums[MAX], n, i = 0;

  while (i < MAX)
    {
      system ("clear");
      printf ("Numero: ");
      scanf ("%i", &n);
      if (n % 2 != 0)
	{
	  nums[i] = n;
	  i++;
	}
    }

  for (int i = 0; i < MAX; i++)
    {
      printf ("%i ", nums[i]);
    }
  printf ("\n");
  return 0;
}
