#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#define FIL 3
#define COL 10

int
main ()
{
  int A[FIL][COL];

  for (int col = 0; col < COL; col++)
    {
      printf ("Numero [1][%i]: ", col);
      scanf ("%i", &A[0][col]);
      A[1][col] = pow (A[0][col], 2);
      A[2][col] = pow (A[0][col], 3);
    }

  system ("clear");

  for (int fil = 0; fil < FIL; fil++)
    {
      for (int col = 0; col < COL; col++)
	{
	  printf ("%i ", A[fil][col]);
	}
      printf ("\n");
    }


  return 0;
}
