#include <stdio_ext.h>
#include <stdlib.h>
#include <string.h>

int
main ()
{

  char frase[20];

  __fpurge (stdin);

  printf ("Escriba una frase: ");
  fgets (frase, 20, stdin);

  int longfrase = strlen (frase);
  frase[longfrase - 1] = '\0';

  printf ("\nFrase introducida: ");
  puts (frase);

  return 0;
}
