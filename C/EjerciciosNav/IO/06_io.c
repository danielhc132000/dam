#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int
main ()
{

  int n1, n2, res;

  printf ("Primer numero a sumar: ");
  scanf ("%i", &n1);

  printf ("Segundo numero: ");
  scanf ("%i", &n2);

  res = n1 + n2;

  printf ("\033[0;36m%i \033[0;37m + \033[0;36m%i \033[0;37m = \033[0;32m%i\n", n1, n2, res);

  return EXIT_SUCCESS;
}
