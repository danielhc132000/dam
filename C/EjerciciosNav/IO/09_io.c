#include <stdio.h>
#include <stdlib.h>

int
main ()
{

  int num;
  printf ("Numero entero: ");
  scanf ("%i", &num);
  printf ("Numero %i: %X\n", num, num);

  return 0;
}
