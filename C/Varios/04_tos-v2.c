#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

bool
preguntar (const char *word)
{

  char resp;

  printf ("%s s/N: ", word);
  scanf (" %c", &resp);

  return tolower (resp) == 's';

}

void
resp (bool tos, bool maulla)
{

if(maulla)
    if(tos)
        printf("Tiene tos felina");
    else
        printf("Eres un gato sano");
else
    if(tos)
        printf("Tiene tos ferina");
    else
        printf("Eres un humano sano");

}

int
main ()
{

  bool tos = preguntar ("tose");
  bool maulla = preguntar ("maulla");

  resp (tos, maulla);


  return EXIT_SUCCESS;
}
