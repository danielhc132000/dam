#include <stdio.h>
#include <stdlib.h>


int main(){
	
	int **n;
	
	n = (int**)malloc(2*sizeof(int*));
	
	*n = (int*)malloc(1*sizeof(int));
	*(n+1) = (int*)malloc(1*sizeof(int));
	
	printf("Pimero numer: ");
	scanf("%i", n);
	
	printf("Pimero numer: ");
	scanf("%i", n+1);
	
	printf("El primer numero es: %i\n", *n);
	printf("El segundo numero es: %i\n", *(n+1));
	
	// printf("La suma de los dos es: %i", *(n)+*(n+1));

	int n1 = *n;
	int n2 = *(n+1);
	
	printf("La suma de los dos es: %i", n1 + n2);
	
	free(n);
	
	return EXIT_SUCCESS;
}
