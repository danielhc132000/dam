#include <stdio.h>
#include <stdlib.h>

#define MAX 10

void
look_nums (char A[]){

  printf ("\n");
  for (int i = 0; i < MAX; i++){
      printf ("%c ", A[i]);
    }
  printf ("\n\n");

}

void
give_nums (char *A){

  for (int i = 0; i < MAX; i++){
      printf("Numero %i: ", i);
      scanf(" %c", A + i);
  }
}

int
main (){
  char A[MAX]; // Matriz de numeros

  //  Entrada de datos
  give_nums (A);

  //  Salida de datos
  look_nums (A);

  return EXIT_SUCCESS;
}
