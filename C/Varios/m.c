#include <stdio.h>
#include <stdlib.h>

#define MAX 10

int
main ()
{

char p1[MAX]; // Array de char con espacio definido.
char *p2; // Caja(variable) vacia "PUNTERO".

printf("Nombre p1: ");  // Pedimos primer string y lo guardamos en el valor de p1 que 
scanf("%s", p1);        // es la direccion del primer espacio del array.

printf("Nombre p2: ");  // Pedimos el segundo string y lo guardamos en la direccion
scanf("%ms", &p2);      // del puntero vacioy y %ms no reserva el espacio de cada char.

printf("\n\n");

printf("Nombre p1: %s\n", p1);  // Imprimimos los dos valores.
printf("Nombre p2: %s\n", p2);  

free(p2);

  return EXIT_SUCCESS;
}
