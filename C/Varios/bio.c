#include <stdio.h>
#include <stdlib.h>
#include <string.h>

const char* menu[] = {  // Crea una lista de char, donde se encuentran las opciones del menu
    "Nombre",
    "Apellidos",
    "Genero",
    "Nacionalidad",
    NULL
};

enum n_menu {NOMBRE, APELLIDO, GENERO, NACIONALIDAD,
             SALIR};    // Crea variables globares con valores comenzando desde el 0

void
pausar(){
    printf("\n\n");
  printf("Pulse para continuar...");
  getchar();
}

void
title(){    // Crea el titulo del programa
  system ("clear");
  system ("toilet -fpagga --gay BIO-GRAFIA");
  printf("\n\n");
}

unsigned
select_opc(){   // Sirve para seleccionar la opcion del menu
    unsigned s_opc;
    printf("Opcion: ");
    scanf("%u", &s_opc);
    
    return s_opc-1;
}

unsigned
bio_menu_fill(){    // Imprime el menu situado en la lista "menu" de arriba
    unsigned i;
    title();
    for(i=0; menu[i]!=NULL; i++)
        printf("%u-%s\n", i+1, menu[i]);
    printf("%u-Salir\n\n", i+1);
    return select_opc();
}


void
fillc(unsigned opc, char** question){   // Sirve para rellenar las diferentes opciones del menu
    printf("Rellene su %s: ", menu[opc]);
    scanf("%ms", question);
}

void    // Segunda parte del programa donde podremos visualizar los datos escritos en la biografia
look_bio(char **name, char **apellidos, char **genero, char **nacionalidad){

    unsigned opc;
    
    do{
        opc = bio_menu_fill();
        switch(opc){
            case NOMBRE:
                printf("\n%s", *name);
                pausar();
                break;
            case APELLIDO:                
                printf("\n%s", *apellidos);
                pausar();
                break;
            case GENERO:                
                printf("\n%s", *genero);
                pausar();
                break;
            case NACIONALIDAD:                
                printf("\n%s", *nacionalidad);
                pausar();
                break;
        }
        getchar();
    }while(opc != SALIR);
}


int 
main(){     // Parte principal del programa metido en un bucle que finaliza al selecionar la opcion salir

    unsigned opc;
    char *name, *apellidos, *genero, *nacionalidad;
    
    do{
        opc = bio_menu_fill();
        switch(opc){
            case NOMBRE:
                fillc(opc, &name);
                break;
            case APELLIDO:
                fillc(opc, &apellidos);
                break;
            case GENERO:
                fillc(opc, &genero);
                break;
            case NACIONALIDAD:
                fillc(opc, &nacionalidad);
                break;
        }
    }while(opc != SALIR);
   
    look_bio(&name, &apellidos, &genero, &nacionalidad); // Llama a la funcion para poder ver los datos de la bio
    
return EXIT_SUCCESS;
}
