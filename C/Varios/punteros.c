#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void
numsref(int* n1, int* n2){

printf("Numero 1: ");
scanf("%i", n1);

printf("Numero 2: ");
scanf("%i", n2);
}

void
numsval(int n1, int n2){
int suma;

suma = n1 + n2;

printf("La suma es: %i", suma);

}

int main(){

int n1, n2, suma;

/* PASO POR REFERENCIA */
printf("*** Suma por Referencia ***\n");

numsref(&n1, &n2);
suma = n1 + n2;

printf("La suma es: %i\n\n", suma);

/* PASO POR  VALOR */
printf("*** Suma por valor ***\n");

printf("Numero 1: ");
scanf("%i", &n1);

printf("Numero 2: ");
scanf("%i", &n2);

numsval(n1, n2);


return EXIT_SUCCESS;
}
