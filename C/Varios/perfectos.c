#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdio_ext.h>


int
main(){

    int sum, c=0;
    
        for(int n=1; n<100000; n++){
            sum=0;
            for(int i=1; i<n; i++){
                if(n%i==0){
                    sum+=i;
                    if(sum == n){
                        printf("%i\n", n);
                        c++; 
                    }
                }
                if(c==5)
                    return EXIT_SUCCESS;
            } 
        }

    return EXIT_SUCCESS;
}
