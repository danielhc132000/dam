#include <stdio.h>
#include <stdlib.h>


int
main(){

    unsigned *n=NULL, pos=0, aux, t;

    do{
        n = (unsigned *)realloc(n, (pos+1)*sizeof(unsigned));
        printf("Numero: ");
        t = scanf("%u", n+pos);
        pos++;
    }while(t);
   

    for(int c=0; c<pos; c++)
        for(int i=0; i<pos; i++)
            if(*(n+i)>*(n+(i+1))){
                aux = *(n+i);
                *(n+i) = *(n+(i+1));
                *(n+(i+1)) = aux;
            }


    for(int r=0; r<pos-1; r++)
       printf("%u ", *(n+r));
    printf("\n");


    return EXIT_SUCCESS;
}
