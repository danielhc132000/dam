#include <stdio.h>

int
main(){

    FILE *temp;

    temp = fopen("temporal.txt", "w");

    if(!temp){
        printf("No se ha podido abrir el archivo.");
        return 1;
    }

    fputc('h', temp);   // Si va bien devuelve el caracter sino devuelve EOF
    fputs("ola", temp); // Si va bien devuelve un numero unsigned sino devuelve un EOF

    fclose(temp);

    return 0;
}

/* fputc es para poder escribir un solo caracter mientras que fputs se utuliza para escribir cadenas
 * de caracteres parecio a fgetc y fgets*/
