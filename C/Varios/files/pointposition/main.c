#include <stdio.h>

int
main(){

    FILE *temperatura;

    temperatura = fopen("temperatura.data", "r");

    if(!temperatura){
        printf("No se ha podido abrir el archivo.");
        return 1;
    }
    
    fseek(temperatura, 3, SEEK_SET);    // Mueve el cursor en el fichero a la posicion indicada, comenzando
    // desde el inicio-SEEK_SET, final-SEEK_END, posicion actual-SEEK_CUR.

    do{
        int car = fgetc(temperatura);
        long pos = ftell(temperatura);  // Devuelve un long int e indica la posicion del cursor en el archivo
        if(car!=EOF)
            printf("%ld %c\n", pos, car);
    }while(!feof(temperatura));


    return 0;
}
