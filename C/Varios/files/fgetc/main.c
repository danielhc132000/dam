#include <stdio.h>

int
main(){

    FILE *temperatura;

    temperatura = fopen("temperatura.data", "r");

    if(!temperatura){
        printf("No se ha podido abrir el archivo.");
        return 1;
    }

    do{
        int leido = fgetc(temperatura); // fgetc obtiene un solo caracter, y a medida que avanza el bucle va cogiendo el siguiente caracter
        if(leido!=EOF)  // == !feof(temperatura)
            printf("%c", (char)leido);
    }while(!feof(temperatura));

    return 0;
}
