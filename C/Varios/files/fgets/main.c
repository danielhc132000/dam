#include <stdio.h>

int
main(){

    FILE *temperatura;
    char save[80];

    temperatura = fopen("temperatura.data", "r");

    if(!temperatura){
        printf("No se ha podido abrir el archivo.");
        return 1;
    }
    
    do{
        fgets(save, 80, temperatura);
        printf("%s", save);
    }while(!feof(temperatura));
    
    fclose(temperatura);

    return 0;
}

/* La diferencia entre fgetc y fgets es que fgetc recoje de caracter en caracter mientras que
 * fgets recoge de string en string parando de coger cuando encuentre un "\0", "\n" o EOF, por ello
 * si no hubiesemos metido la funcion fgets en un bucle, solo imprimiria la primera cadena "Madrid"*/
