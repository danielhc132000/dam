#include <stdio.h>
#include <stdlib.h>

const char* options[] = {   // CREA UNA LISTA/ARRAY CON CONSTANTES DE TIPO CARACTER(DIRECCION DEL PRIMER CARACTER)
  "Sumar",
  "Restar",
  "Multiplicar",
  "Dividir",
  "Salir",
  NULL
};

enum Noptions{SUMAR=1, RESTAR=2, MULTIPLICAR=3, DIVIDIR=4, SALIR=5};  // CREA CONSTANTES GLOBALES

void
pulsar(){
  printf("\n\nPulsa una tecla para continuar");
  getchar();
}

void
select_opt(unsigned *opt){  // SELECCIONA UNA OPCION DEL MENU
  printf("\n\tOpcion: ");
  scanf("%u", opt);
  printf("\n");
}


void
title(){    // IMPRIME EL TITULO DEL PROGRAMA
  system ("clear");
  system ("toilet -fpagga --gay SUPERCALCULATOR");

   printf ("\n\n");

}


void
menu(){   // IMPRIME LAS OPCIONES DEL PROGRAMA QUE ESTAN EN LA LISTA DE CONST CHAR* OPTIONS

int i=0;
    title();    
  
      for(; options[i]!=NULL; i++)
        printf("\t%i-%s\n", i+1, options[i]);
}

void
pedir_nums(int *n1, int *n2){   //PEDIR NUMEROS PARA OPERAR
  printf("Primer numero: ");
  scanf("%i", n1);

  printf("Segundo numero: ");
  scanf("%i", n2);
}

    /* FUNCIONES QUE REALIZAN LAS OPERACIONES */

void
oper_sum(){   // FUNCION PARA EL CASE:SUMAR
  
  int n1, n2, suma;

  pedir_nums(&n1, &n2);
  suma = n1 + n2;

  printf("%i + %i = %i", n1, n2, suma);
}


void
oper_rest(){    // FUNCION PARA EL CASE:RESTAR

  int n1, n2, resta;

  pedir_nums(&n1, &n2);
  resta = n1 - n2;

  printf("%i - %i = %i", n1, n2, resta);
}

void
oper_mult(){    // FUNCION PARA EL CASE:MULTIPLICAR

  int n1, n2, multi;

  pedir_nums(&n1, &n2);
  multi = n1 * n2;

  printf("%i * %i = %i", n1, n2, multi);
}


void
oper_div(){     // FUNCION PARA EL CASE:DIVIDIR

  int n1, n2, division;

  pedir_nums(&n1, &n2);
  division = n1 / n2;

  printf("%i / %i = %i", n1, n2, division);
}


int   
main ()   // FUNCION PRINCIPAL CON EL PROGRAMA EN UN BUCLE QUE FINALIZA SI SELECIONAMOS EXIT=5
{
  unsigned opt;

do{
    menu();
    select_opt(&opt);
    switch(opt){
        case SUMAR:
          oper_sum();
          pulsar();
          break;
        case RESTAR:
          oper_rest();         
          pulsar();
          break;
        case MULTIPLICAR:
          oper_mult();
          pulsar();
          break;
        case DIVIDIR:
          oper_div();        
          pulsar();
          break;
        default:
          printf("Elige una de las opciones disponibles");
          pulsar();
    }

getchar();
}while(opt != SALIR);


  return EXIT_SUCCESS;

}

