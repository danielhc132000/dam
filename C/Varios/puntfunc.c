#include <stdio.h>
#include <stdlib.h>

const char *options[] = {
    "Sumar", "Restar", "Multiplicar","Dividir", NULL 
};


double suma(double n1, double n2){ return n1 + n2; }
double resta(double n1, double n2){ return n1 - n2; }
double multi(double n1, double n2){ return n1 * n2; }
double divi(double n1, double n2){ return n1 / n2; }

double (*funcion[])(double n1, double n2) = {&suma, &resta, &multi, &divi}; //  Puntero a un array que contiene las direcciones de funciones(Puntero a funciones)
//  Contiene el dato de retorno de las funcines, la direccion de la funcion y los parametros de la misma

void
title(){

    system("clear");
    system("toilet -fpagga 'Operaciones'");
    printf("\n\n");
}


void selection(int *op){
    printf("\n\nSelecione una opcion: ");
    scanf("%i", op);
}


void
menu(int *op){

    title();    //  Titulo
    for(int i=0; options[i]!=NULL; i++)
        printf("%i.-%s\n", i, options[i]);

    selection(op); //  Seleccionar la opcion
}

void
pedir_numeros(double *n1, double *n2){

    printf("Primer numero: ");
    scanf("%lf", n1);

    printf("Segundo numero: ");
    scanf("%lf", n2);
}


void
operacion(int op){
    double n1, n2, total;

    pedir_numeros(&n1,&n2);
    total = (*funcion[op])(n1, n2);
    printf("\n%.2lf", total);
}


int
main(){

    int op;
    
    menu(&op);
    operacion(op);

    return EXIT_SUCCESS;
}
