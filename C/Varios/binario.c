#include <stdio.h>
#include <stdlib.h>


void
binario(unsigned n){

    if (n>0)
        binario(n /  2);
    printf("%u", n % 2);

}


int
main(){

    unsigned num;

    printf("Numero: ");
    scanf("%u", &num);

    binario(num);

    return EXIT_SUCCESS;
}
