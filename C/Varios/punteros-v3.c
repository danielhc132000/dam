#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int
main(){

    // PUNTEROS BASICOS

    int a =12;  // Guardamos en a el valor de 12
    int *p_a = &a;  // Apuntamos con *p_a a la direccion de a
    int *p_a2 = p_a;    // Apuntamos *p_a2 al valor de p_a

    printf("Valor de a: %i\n", a);  // Valor que almacena la variable a
    printf("Direccion de a: %p\n", &a); // Direccion donde se encuentra la variable a
    printf("Valor de *p_a: %i\n", *p_a);    // Valor de alli donde apunta p_a que es el valor de la direccion de a
    printf("Valor de p_a: %p\n", p_a);  // La direccion de p_a es la misma que la de a ya que los punteros son como los arrays
    printf("Valor de *p_a2: %i\n", *p_a2);  // El valor de p_a2 es 12 ya que apunta al valor de p_a que es la direccion de a

    printf("\n\n");

    // PUNTEROS CON STRINGS

    char *c = malloc(sizeof(char) * 5); // Malloc reserva 5 celdas char, y el puntero *c apunta a la primera celda resevada
    
    strcpy(c, "Hello"); // Copiamos en c(primera celda reservada) la cadena "Hello"
    printf("%s\n\n", c);    // Imprimimos el valor de c, que es "Hello"   
    free(c);    // Liberamos la memoria reservada en c

    char **p = malloc(sizeof(char*)*2); // Reserva 2 celdas de punteros a chars, y **p apunta la la primera celda reservada

    char *string1 = malloc(sizeof(char)* 5);    // Reserva 5 celdas de chars, y *string1 apunta la la primera celda reservada
    char *string2 = malloc(sizeof(char)* 6);    // Reserva 6 celdas de chars, y *string2 apunta la la primera celda reservada

    strcpy(string1, "Hola");    // Copiamos en string1 la cadena "Hola"
    strcpy(string2, "Adios");   // Copiamos en string2 la cadena "Adios"

    *p = string1;   // Hacemos que la primera celda reservada de *p apunte al puntero string1
    *(p+1) = string2;   // Hacemos que la segunda celda reservada de *p apunte al puntero string2

    printf("%s\n", *(p));   // Imprimimos alli a donde apunta *(p), que es la primera celda reservada p[0]
    printf("%s\n", *(p+1)); // Imprime alli donde apunta *(p+1), es decir, la segunda celda reservada p[1]

    free(string1);  // Liberamos la memoria reservada en string1
    free(string2);  // Liberamos la memoria reservada en string2
    free(p);    // Liberamos la memoria reservada en p



    return EXIT_SUCCESS;
}
