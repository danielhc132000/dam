#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void
n_sucesion(unsigned *n_sucesion){   // Preguntamos la longitud de la sucesion

    printf("Longitud de la sucesion: ");
    scanf("%i", n_sucesion);
}

void
rellenar_array(unsigned A[], unsigned longi){

    for(unsigned i=2; i<longi; i++)   // Rellenamos el resto del array
        A[i] = A[i-1] + A[i-2];
}

void
mostrar_array(unsigned A[], unsigned longi){

    for(unsigned i=0; i<longi; i++)   // Imprimimos el array completo
        printf("%u ", A[i]);
}

int main(){
    unsigned longi;

    n_sucesion(&longi);

    unsigned A[longi];
    
    A[0]=A[1]=1;    // Asigno el valor 1 a las dos primeras celdas del array

    rellenar_array(A, longi);
    mostrar_array(A, longi);
   
return EXIT_SUCCESS;
}
