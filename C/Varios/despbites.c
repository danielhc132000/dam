#include <stdio.h>
#include <stdlib.h>


int
main(int arg, char *argv[]){

// DESPLAZAMIENTO DE BITES

printf("16 >> 1 = %i\n", 16 >> 1); // 1 0000 --> 1000 = 8
printf("16 >> 2 = %i\n", 16 >> 2); // 1 0000 --> 0100 = 4
printf("16 >> 3 = %i\n", 16 >> 3); // 1 0000 --> 0010 = 2
printf("16 >> 4 = %i\n", 16 >> 4); // 1 0000 --> 0001 = 1
printf("\n");
printf("1 << 1 = %i\n", 1 << 1); //   0001  -->  0010 = 2
printf("1 << 2 = %i\n", 1 << 2); //   0001  -->  0100 = 4
printf("1 << 3 = %i\n", 1 << 3); //   0001  -->  1000 = 8
printf("1 << 4 = %i\n", 1 << 4); //   0001  -->1 0000 = 16

printf("\n\n");

// MULTIPLICAR POR DOS

for (int i=0; i<10; i++)
	printf("%i ", i<<1);
	

return EXIT_SUCCESS;
}

