#include <stdio.h>
#include <stdlib.h>

#define DX 0.0005

double f1 (double x) { return - (x * x) + 3; }  // Funciones que devuelven el resultado de  una operacion
double f2 (double x) { return - (x * x) + 7; }

double integra (  // ------PARAMETROS--------
        double (*pf)(double), // Puntero a funcion que retorna un double y recibe un double
        const double LI,  // Dos variables doubles con valores constantes
        const double LS
        )
{
    double r = 0;

    for (double x=LI; x<LS; x+=DX)
        r += (*pf) (x);

    r *= DX;

    return r;
}


int main (int argc, char *argv[]) {

    system ("clear");
    system ("toilet -fpagga --gay Integrales");

    printf ("I1(0,3) = %.2lf\n", integra (&f1, 0, 3)); /* Llamamos a la funcion y como primer parametro mandamos la direccion
                                                        de la primera  funcion a la que apuntara el puntero pf*/
    printf ("I2(0,3) = %.2lf\n", integra (&f2, 0, 3));

    return EXIT_SUCCESS;
}

