#include <stdio.h>
#include <stdlib.h>
#include <string.h>


void
print(int tab[3][3]){
  for(int f=0; f<3; f++){
      for(int c=0; c<3; c++)
          printf("%i ", tab[f][c]);
      printf("\n");
  }
}

int
main(){

    int *puntero;

    puntero = (int*)malloc((3*3)*sizeof(int));
    bzero(puntero, sizeof(puntero));

    print((int(*)[3])puntero);  // Castea un puntero a un array bidimensional de 3 filas


  return EXIT_SUCCESS;
}
