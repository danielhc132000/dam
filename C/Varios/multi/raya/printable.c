#include <stdio.h>
#include <stdlib.h>
#include "proto.h"


void
title(){
    system("clear");
    system("toilet -f smblock --filter border:metal 'Bienvenido a tres en raya'");
    printf("\n\n");
}


void
print(int tab[MAX][MAX]){
    printf("+---+---+---+\n");
    for(int f=0; f<MAX; f++){
        for(int c=0; c<MAX; c++)
            printf("| %i ", tab[f][c]);
        printf("|\n+---+---+---+\n");
    }
}


void
preguntar(int *fil, int *col, int *t){
    *t = turno(*t);
    printf("\nTurno del jughador %i.", *t);
    printf("\n\nSeleccione la fila y la columna: ");
    scanf("%i %i", fil, col);
}
