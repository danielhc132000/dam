#include <stdio.h>
#include "proto.h"


int 
turno(int t){
    return t % 2 == 0 ? 1 : 2;
}


void
cambio(int tab[MAX][MAX], int t, int fil, int col){
    if(tab[fil-1][col-1]==0){
        if(t==1)
            tab[fil-1][col-1] = J1;
        else
            tab[fil-1][col-1] = J2;
    }
}


int
win_h(int tab[MAX][MAX]){
    for(int f=0; f<MAX; f++){
        for(int c=0; c<MAX; c++){
            if(tab[f][c]==1 && tab[f][c+1]==1 && tab[f][c+2]==1){
                printf("\nEl jugador 1 ha ganado completando la fila %i.", f+1);
                return 1;
            }
            if(tab[f][c]==2 && tab[f][c+1]==2 && tab[f][c+2]==2){
                printf("\nEl jugador 2 ha ganado completando la fila %i.", f+1);
                return 1;
            }
        }
    }
}


int
win_v(int tab[MAX][MAX]){
    for(int c=0; c<MAX; c++){
        for(int f=0; f<MAX; f++){
            if(tab[f][c]==1 && tab[f+1][c]==1 && tab[f+2][c]==1){
                printf("\nEl jugador 1 ha ganado completando la columna %i.", c+1);
                return 1;
            }
            if(tab[f][c]==2 && tab[f+1][c]==2 && tab[f+2][c]==2){
                printf("\nEl jugador 2 ha ganado completando la columna %i.", c+1);
                return 1;
            }
        }
    }
}


int
win(int tab[MAX][MAX]){
    int w=0;
    w = win_h(tab);
    w = win_v(tab);

    return w;
}
