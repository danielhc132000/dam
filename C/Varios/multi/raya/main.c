#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "proto.h"

int
main(){

    int tab[MAX][MAX];
    int fil,col, c=0, t=0, w=0;

    bzero(tab, sizeof(tab));

    do{
        title();
        print(tab);
        preguntar(&fil, &col, &t);
        cambio(tab, t, fil, col);
        w = win(tab);
        if(w==1)
            break;
        c++;
    }while(c<9);

    return EXIT_SUCCESS;
}
