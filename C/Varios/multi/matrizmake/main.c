#include <stdio.h>
#include <stdlib.h>
#include "header.h"


int
main (){
  int *A; // Matriz de numeros
  A = (int*)malloc(MAX*sizeof(int));
  
  //  Entrada de datos
  give_nums (A);

  //  Salida de datos
  look_nums (A);

  return EXIT_SUCCESS;
}
