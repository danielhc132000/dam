#include <stdio.h>
#include <stdio_ext.h>
#include <stdlib.h>


const char* menu_opt[] = {
    "Square empty",
    "Square with diagonal",
    "Filled square",
    "Exit",
    NULL
};

enum options {EMPTY, DIAGONAL, FILLED, EXIT};

void
pause(){
    printf("\nPulse para continuar...");
    getchar();
    __fpurge(stdin);
}


void
title(){

system("clear");
system("toilet -f future SQUARES --filter metal");
printf("\n\n");
}


unsigned
select_option(){
    unsigned opt;

    printf("\nOption: ");
    scanf("%u", &opt);
    __fpurge(stdin);

    return opt-1;
}



unsigned
print_menu(){
 
    title();
    for(int i=0; menu_opt[i] != NULL; i++ ){
        printf("%i-%s\n", i+1, menu_opt[i]);
    }
    
    return select_option();
}

int
width(){
    unsigned l;

    printf("Longitud: ");
    scanf("%i", &l);
    __fpurge(stdin);

    printf("\n");

    return l;
}


bool
is_border(int f, int c, int l){
    return f == 0 || f == l - 1 || c == 0 || c == l - 1; 
}


bool
is_borderD(int f, int c, int l){
    return f == c || f == 0 || f == l - 1 || c == 0 || c == l - 1; 
}

void
q_empty(){

    int l = width();
    

    for(int f=0; f<l; f++){
        for(int c=0; c<l; c++)
            if(is_border(f, c, l))
                printf("* ");
            else
                printf("  ");
        printf("\n");
    }
    pause();
}


void
q_diagonal(){

    int l = width();
    
    for(int f=0; f<l; f++){
        for(int c=0; c<l; c++)
            if(is_borderD(f, c, l))
                printf("* ");
            else
                printf("  ");
        printf("\n");
    }
    pause();
}


void
q_filled(){

    int l = width();
    
    for(int f=0; f<l; f++){
        for(int c=0; c<l; c++)
                printf("* ");
        printf("\n");
    }
    pause();
}


void
square(){

    unsigned opt;

    do{
    opt = print_menu();
    switch(opt){
        case EMPTY:
            q_empty();
            break;
        case DIAGONAL:
            q_diagonal();    
            break;
        case FILLED:
            q_filled();
            break;
    }
    }while(opt != EXIT);

}


int
main(){

    square();

    return EXIT_SUCCESS;
}
/*
 *
 * Refactorizar los for en los que cambia solo el if haciendo un solo if en el que ponemos las dos
 * funciones de condiciones separados de un || y todo ello metido en un condicional
 *
 * */
