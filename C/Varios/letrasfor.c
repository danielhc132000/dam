#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void
sucesion(unsigned num){
    char letter = 'a';

    for(int i=0; i<num; i++){
        printf("%c", letter++);
    }
}

void
preguntar_num(){
    unsigned num;

    printf("Numero de letras de la sucesion: ");
    scanf("%u", &num);
    sucesion(num);
}

int main(){

preguntar_num();


return EXIT_SUCCESS;
}
