#include <stdio.h>
#include <stdlib.h>



int 
main(){

    unsigned l;

    printf("Longitud del tablero: ");
    scanf("%u", &l);

    for(int f=0; f<l; f++){
        for(int f2=0; f2<l; f2++){
            for(int c=0; c<l; c++){
                for(int c2=0; c2<l; c2++){
                    if((f+c)%2!=0)
                        printf("1 ");
                    else
                        printf("0 ");
                }
            }
        printf("\n");
        }
    }


    return EXIT_SUCCESS;

}
