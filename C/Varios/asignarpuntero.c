#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void
asignar_valor(int *n1, int *n2){

    scanf ("%i", n1);   // Mandamos el valor del scanf al valor de n1, que es la direccion del n1 del main
    *n2 = 10;   // Asignamos el valor de diez a "alli donde apunta" n2 que es la direccion de n2 del main

}


int main(){

int n1, n2, suma;

asignar_valor(&n1, &n2);

suma = n1 + n2;

printf("La suma de %i y %i es: %i", n1, n2, suma);

return EXIT_SUCCESS;
}
