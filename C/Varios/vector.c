#include <stdio.h>
#include <stdlib.h>


int
main(){

    double *vector;
    unsigned longitud;

    printf("Longitud del vector: ");
    scanf("%u", &longitud);

    vector = (double*) malloc (sizeof(double)*longitud);

    for(int i=0; i<longitud; i++){
        printf("Numero: ");
        scanf("%lf", vector+i); // Sumamos 8 bits a vector ya que es un double, y no le sumamos uno que es lo que vale i,
                                // i realmente indica la cantida de elementos en bites a sumar
    }
    
    printf("\n");

    for(int i=0; i<longitud; i++){
        printf("%i ---> %.3lf\n", i, *(vector+i));  // Imprimimos el contenido de alli donde aputa vector
    }

    free(vector);

    return EXIT_SUCCESS;
}

