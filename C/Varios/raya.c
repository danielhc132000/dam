#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX 3

const char *representa[] = {
    "  ",
    "⭕", // 2B55
    "❌", // 274C
};


void
imprimir(unsigned tab[MAX][MAX]){ // Imprime el tablero

    for(int f=0; f<MAX; f++){ 
        printf("\t----------------");
        printf("\n\t┃");
        for(int c=0; c<MAX; c++){
            printf(" %s ┃", representa[tab[f][c]]);
        }
        printf("\t\n");
    }
    printf("\t----------------"); 
    printf("\n");
}

void
title(){  // Imprime un titulo del juego
  system ("clear");
    system ("toilet -Fborder -fpagga --metal 'TRES EN RAYA'");
    printf ("\n\n");

}

unsigned
turno(unsigned t){  // Sirve para indicar los turnos de los jugadores
    return t % 2 == 0 ? 1 : 2; 
}


void
cambiar(unsigned tab[MAX][MAX], unsigned fil, unsigned col, unsigned t){  // Cambia los espacios del tablero por las fichas de los jugadores

    if(tab[fil][col] == 0){
        if(t == 1){
            tab[fil][col] = 1;
        }else{
            tab[fil][col] = 2;
        }
    }
}


void
quest(unsigned t, unsigned *fil, unsigned *col){  // Pregunta la posicion y la guarda

    printf("\tTurno Jugador %u\n", t);
    printf("\tFila y Columna: ");
    scanf("%u %u", fil, col);
}


void
win_h(unsigned tab[MAX][MAX], unsigned *w){ // Indica si gana un jugador haciendo una fila en horizontal
    for(int f=0; f<MAX; f++){
        for(int c=0; c<MAX; c++){
            if(tab[f][c]==1 && tab[f][c+1] == 1 && tab[f][c+2]==1){
                printf("\nHa ganado el jugador 1, ha completado la fila %i ✅", f+1);
                *w = 1;
            }
            if(tab[f][c]==2 && tab[f][c+1]==2 && tab[f][c+2]==2){
                printf("\nHa ganado el jugador 2, ha completado la fila %i ✅", f+1);
                *w = 2;
            }
        }
    }
}

void
win_v(unsigned tab[MAX][MAX], unsigned *w){ // Indica si gana un jugador en una fila en vertical

    for(int c=0; c<MAX; c++){
        for(int f=0; f<MAX; f++){
            if(tab[f][c]==1 && tab[f+1][c] == 1 && tab[f+2][c]==1){
                printf("\nHa ganado el jugador 1, ha completado la columna %i ✅", c+1);
                *w = 1;
            }
            if(tab[f][c]==2 && tab[f+1][c]==2 && tab[f+2][c]==2){
                printf("\nHa ganado el jugador 2, ha completado la columna %i ✅", c+1);
                *w = 2;
            }
        }
    }
}

void
jugar(unsigned tab[MAX][MAX]){

    unsigned w=0, t=0, c=0, fil, col; // t = turno, c = coIntador de celdas, al llegar a 9 "celdas" el bucle finaliza
    imprimir(tab);

    title();

    do{
        title();
        imprimir(tab);
        win_h(tab, &w);
        win_v(tab, &w);
        if(w!=0)
            break;
        t = turno(t);
        quest(t, &fil, &col);
        cambiar(tab, fil-1, col-1, t);
        imprimir(tab);
        c++;
    }while(c!=MAX*3);
    printf("\n\n");
}

int
main (){

    unsigned tab[MAX][MAX];

    bzero(tab, sizeof(tab));  // Relleno el vector de ceros
    jugar(tab);

    return EXIT_SUCCESS;
}
