#include <stdio.h>
#include <stdlib.h>
#include <string.h>


struct RGB {
    int r;
    int g;
    int b;
};


void
rgb_set(struct RGB *rgb, int r, int g, int b){

    if(r<=255 && g<=255 && b<=255 && r>=0 && g>=0 && b>= 0){
        rgb->r = r;
        rgb->g = g;
        rgb->b = b;
    }else{
        printf("Algun numero no esta en el rango 0-255");
        exit(-1);
    }
}


void
rgb_invert(struct RGB *rgb){

    rgb_set(rgb, 255 - rgb->r, 255 - rgb->g, 255 - rgb->b);
}


char
*rgb_str(struct RGB *rgb){

    char *str = malloc(sizeof(char) * 64);
    sprintf(    //  Guarda en el puntero str una cadena con los valores de un struct
        str, "RGB: (%i, %i, %i)\nHEX: 0x%x%x%x",
        rgb->r, rgb->g, rgb->b,
        rgb->r, rgb->g, rgb->b
    );

    return str;
}


struct RGB *get_inverted_colors(char *file, int *limitp){

    int limit = *limitp;
    struct RGB *inverted = malloc(sizeof(struct RGB) * limit);

    FILE *f = fopen(file, "r");

    int line_size = 16;
    char *line = malloc(sizeof(char) * line_size);
    
    int i = 0;
    while(fgets(line, line_size, f)) {
        
        if(i >= limit){
            limit++;
            inverted = realloc(inverted, sizeof(struct RGB) * limit);
        }

        int values[3]; 
        char *split = strdup(line); //  Duplica una cadena
        
        for(int i=0; i<3; i++){
            values[i] = atoi(strsep(&split, " "));
        }
       
        free(split);

        rgb_set(&inverted[i], values[0], values[1], values[2]);
        rgb_invert(&inverted[i++]);
    }
    
    free(line);
    fclose(f);
    *limitp = limit;
    return inverted;
}


int main(int argc, char *argv[]){
   
    if(argc!=2) {
        printf("Error, solo permite un argumento");
        return EXIT_FAILURE;
    }
    
    int limit = 4;
    struct RGB *inverted = get_inverted_colors(argv[1], &limit);

    for(int i = 0; i < limit; i++){
        char *s = rgb_str(&inverted[i]);
        printf("%s\n\n", s);
        free(s);
    }

    free(inverted);


    return EXIT_SUCCESS;
}
