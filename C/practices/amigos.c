#include <stdio.h>
#include <stdlib.h>
#include <string.h>


unsigned
parar(char **l, unsigned cant){
    return strcmp(*(l+(cant-1)), "fin");
}

void
imprimir(char **l, unsigned cant){
    system("clear");
    
    for(int i=0; i<cant-1; i++)
        printf("- %s\n", *(l+i));
}


int
main(){

    char **l=NULL;
    unsigned cant=0;

    do{
        l = (char**)realloc(l, (cant+1)*sizeof(char*));
        printf("Amigo: ");
        scanf("%ms", l+cant);
        cant++;
    }while(parar(l, cant));

    imprimir(l, cant); 
    free(l);

    return EXIT_SUCCESS;
}
