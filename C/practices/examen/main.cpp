#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "interfaz.h"

#define MAX 8

enum TPieza{vacio,peones,torres,caballos,alfiles,reinas,reyes};


void
colocar(int tablero[MAX][MAX]){

    for(int f=0; f<MAX; f++){
        for(int c=0; c<MAX; c++){
            if(f==0||f==1||f==6||f==7){
                if(c%2==0)
                    tablero[f][c] = peones;
                else
                    tablero[f][c] = torres;
            }

        }

    }

}


void
print(int tab[MAX][MAX]){

  for(int f=0; f<MAX; f++){
      for(int c=0; c<MAX; c++)
          printf("%i ", tab[f][c]);
    printf("\n");
  }
}


int
main(int argc, char **argv){

    int *tablero_negras;
    int col, fil;

    tablero_negras = (int*)malloc((MAX*MAX)*sizeof(int));
    bzero(tablero_negras, sizeof(tablero_negras));


    colocar((int (*)[8])tablero_negras);  // Castea un puntero unidimensional a bidimensional

    preguntar_posicion(&col, &fil);

    print((int(*)[8])tablero_negras);

    free(tablero_negras);


    return EXIT_SUCCESS;

}
