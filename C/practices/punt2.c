#include <stdio.h>
#include <stdlib.h>


void
print(char **p_nomape){

    printf("\nMi nombre es: %s\n", *(p_nomape));
    printf("Mi apellido es: %s", *(p_nomape + 1));
}


void
rellenar(char ***p_nomape){

    *(p_nomape) = malloc(sizeof(char*) * 2);

    printf("Nombre: ");
    scanf("%ms", *(p_nomape));
    
    printf("Apellido: ");
    scanf("%ms", *(p_nomape + 1));
}

int
main(){

    char **nomape;

    rellenar(&nomape);
    print(nomape);

    free(nomape);


    return EXIT_SUCCESS;
}


// Programa de una funcion que apunta al valor del puntero del main
