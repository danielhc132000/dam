#include <stdio.h>
#include <stdlib.h>


void
print(char **nomape){

    printf("\nMi nombre es: %s\n", *(nomape));
    printf("Mi apellido es: %s", *(nomape + 1));
}

int
main(){

    char **nomape;

    nomape = malloc(sizeof(char*) * 2);

    printf("Nombre: ");
    scanf("%ms", nomape);
    
    printf("Apellido: ");
    scanf("%ms", nomape + 1);

    print(nomape);

    free(nomape);


    return EXIT_SUCCESS;
}
