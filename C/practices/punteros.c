#include <stdio.h>
#include <stdlib.h>


int
main(){

    int var = 10;
    int *p_var = &var;

    printf("Valor de var: %i\n", var);  // Valor que contine var
    printf("Alli donde apunta *p_var: %i\n", *p_var);   // Valor de alli donde apunta *p_var
    printf("Direccion de var: %p\n", &var); // Direccion de memoria de var
    printf("Valor de *p_var: %p\n\n", p_var);   // Valor de p_var, apunta a la direccion de var


    char *word;
    word = malloc(sizeof(char)*7); // Apuntamos a una cadena de 7 chars (el untimo es un "\0")

    word = "Daniel";    // Guardamos una cadena en las celdas
    printf("Mi nombre es: %s\n\n", word);   // Imprimimos la cadena



    char **string_word; // Creo un puntero que apunta a punteros que apunta a celdas de tipo char

    string_word = malloc(sizeof(char*) * 3);    // Creamos tres celdas de punteros a char

    printf("Primera palabra: ");
    scanf("%ms", string_word);  // Pedimos la primera palabra y reservamos las celdas de las letras con %ms
    
    printf("segunda palabra: ");
    scanf("%ms", string_word + 1);  // Pedimos la segunda palabra y reservamos las celdas de las letras con %ms
    
    printf("Tercera palabra: ");
    scanf("%ms", string_word + 2);  // Pedimos la tercera palabra y reservamos las celdas de las letras con %ms

    printf("%s", *(string_word));
    printf("%s", *(string_word + 1));   // Imprimimos las palabras y sumando 1 movemos la posicion del puntero **string_word
    printf("%s", *(string_word + 2));

    return EXIT_SUCCESS;
}
