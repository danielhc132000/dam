#include <stdio.h>
#include <stdlib.h>

struct LDatos {
    char *name;
    char password[10];
    unsigned edad;
};


void
give(struct LDatos *usuario){

    printf("User: ");
    scanf("%ms", &usuario->name);

    printf("Password: ");
    scanf("%s", usuario->password); 

    printf("Years Old: ");
    scanf("%u", &usuario->edad);
}

void
print(struct LDatos usuario){
    printf("El user es: %s\n", usuario.name);
    printf("El passwor es: %s\n", usuario.password);
    printf("Su edad es: %u", usuario.edad);
}

int
main(){

    struct LDatos user;

    give(&user);
    print(user);


    return EXIT_SUCCESS;
}
